CREATE TABLE IF NOT EXISTS `awg_bonus_account` (
  `ID` int(11) NOT NULL,
  `BONUS` float NOT NULL,
  `DATA` DATE NULL,
  PRIMARY KEY (`ID`)
)
ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `awg_bonus_log` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DATA` TIMESTAMP,
  `USER_ID` int(11) NOT NULL,
  `TYPE` int(11) NOT NULL,
  `BONUS` float NOT NULL,
  PRIMARY KEY (`ID`)
)
ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `awg_bonus_count` (
  `ID` int(11) NOT NULL,
  `COUNT` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
)
ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `awg_bonus_ticket` (
  `ID` int(11) NOT NULL,
  `ID_EVENT_B` int(11),
  `ID_EVENT_T` int(11),
  `DISABLE` tinyint(1) default NULL,
  `COUNT` int(11),
  `PERCENT` int(11),
  PRIMARY KEY (`ID`)
)
ENGINE=InnoDB;