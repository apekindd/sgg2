<?php

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Config\Option;

$module_id = 'awg.bonus';

Loc::loadMessages($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/options.php");
Loc::loadMessages(__FILE__);

if ($APPLICATION->GetGroupRight($module_id) < "S"){
	$APPLICATION->AuthForm(Loc::getMessage("ACCESS_DENIED"));
}

\Bitrix\Main\Loader::includeModule($module_id);

$request = \Bitrix\Main\HttpApplication::getInstance()->getContext()->getRequest();

;

$iblocks_list = function($IBLOCK_TYPE){
	CModule::IncludeModule("iblock");
	$select = Array();
	$select[] = GetMessage("AWG_BONUS_SELECT");
	$filter = Array();
	$filter["TYPE"] = $IBLOCK_TYPE != "-" ? $IBLOCK_TYPE : "";
	$rsIBlocks = CIBlock::GetList(array("NAME" => "ASC"), $filter);
	while ($arIBlock = $rsIBlocks->Fetch()){
		$select[$arIBlock["ID"]] = $arIBlock["NAME"];
	}

	return $select;
};



$aTabs = array(
	Array(
		'DIV'     => 'NASTROYKI',
		'TAB'     => Loc::getMessage('AWG_BONUS_TAB_NASTROYKI'),
		'OPTIONS' => Array(
			array('bonus_registr', Loc::getMessage('AWG_BONUS_OPTION_BONUS_REGISTR_TITLE'), Loc::getMessage('AWG_BONUS_OPTION_BONUS_REGISTR_DEFAULT_VALUE'), array('text', 0)),
			array('bonus_ticket', Loc::getMessage('AWG_BONUS_OPTION_BONUS_TICKET_TITLE'), Loc::getMessage('AWG_BONUS_OPTION_BONUS_TICKET_DEFAULT_VALUE'), array('text', 0)),
			array('bonus_anketa', Loc::getMessage('AWG_BONUS_OPTION_BONUS_ANKETA_TITLE'), Loc::getMessage('AWG_BONUS_OPTION_BONUS_ANKETA_DEFAULT_VALUE'), array('text', 0)),
			array('bonus_ref', Loc::getMessage('AWG_BONUS_OPTION_BONUS_REF_TITLE'), Loc::getMessage('AWG_BONUS_OPTION_BONUS_REF_DEFAULT_VALUE'), array('text', 0)),
			array('count_tickets', Loc::getMessage('AWG_BONUS_OPTION_COUNT_TICKETS_TITLE'), Loc::getMessage('AWG_BONUS_OPTION_COUNT_TICKETS_DEFAULT_VALUE'), array('text', 0)),
			
			array('years', Loc::getMessage('AWG_BONUS_OPTION_YEARS_TITLE'), Loc::getMessage('AWG_BONUS_OPTION_YEARS_DEFAULT_VALUE'), array('text', 0)),
			array('months', Loc::getMessage('AWG_BONUS_OPTION_MONTHS_TITLE'), Loc::getMessage('AWG_BONUS_OPTION_MONTHS_DEFAULT_VALUE'), array('text', 0)),
			array('days', Loc::getMessage('AWG_BONUS_OPTION_DAYS_TITLE'), Loc::getMessage('AWG_BONUS_OPTION_DAYS_DEFAULT_VALUE'), array('text', 0)),
			
			array('enable_bonus', Loc::getMessage('AWG_BONUS_OPTION_ENABLE_BONUS_TITLE'), '', array('checkbox', "Y")),
			array('infoblok', Loc::getMessage('AWG_BONUS_OPTION_INFOBLOK_TITLE'), '', array('selectbox', $iblocks_list())),
			array('infoblok_anketa', Loc::getMessage('AWG_BONUS_OPTION_INFOBLOK_ANKETA_TITLE'), '', array('selectbox', $iblocks_list())),		),
	),

	array(
		"DIV"     => "rights",
		"TAB"     => Loc::getMessage("MAIN_TAB_RIGHTS"),
		"TITLE"   => Loc::getMessage("MAIN_TAB_TITLE_RIGHTS"),
		"OPTIONS" => Array()
	),
);
#Сохранение

if ($request->isPost() && $request['Apply'] && check_bitrix_sessid()){

	foreach ($aTabs as $aTab){
		foreach ($aTab['OPTIONS'] as $arOption){
			if (!is_array($arOption))
				continue;

			if ($arOption['note'])
				continue;


			$optionName = $arOption[0];

			$optionValue = $request->getPost($optionName);

			Option::set($module_id, $optionName, is_array($optionValue) ? implode(",", $optionValue) : $optionValue);
		}
	}
}

$tabControl = new CAdminTabControl('tabControl', $aTabs);

?>
<? $tabControl->Begin(); ?>
<form method='post'
	  action='<? echo $APPLICATION->GetCurPage() ?>?mid=<?=htmlspecialcharsbx($request['mid'])?>&amp;lang=<?=$request['lang']?>'
	  name='awg_bonus_settings'>

	<? foreach ($aTabs as $aTab):
		if ($aTab['OPTIONS']):?>
			<? $tabControl->BeginNextTab(); ?>
			<? __AdmSettingsDrawList($module_id, $aTab['OPTIONS']); ?>

		<? endif;
	endforeach; ?>

	<?
	$tabControl->BeginNextTab();

	require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/admin/group_rights.php");

	$tabControl->Buttons(); ?>

	<input type="submit" name="Apply" value="<? echo GetMessage('MAIN_SAVE') ?>">
	<input type="reset" name="reset" value="<? echo GetMessage('MAIN_RESET') ?>">
	<?=bitrix_sessid_post();?>
</form>
<? $tabControl->End(); ?>

