<?
$MESS["AWG_BONUS_MODULE_NAME"] = "Бонусная система";
$MESS["AWG_BONUS_MODULE_DESC"] = "";
$MESS["AWG_BONUS_PARTNER_NAME"] = "AWG";
$MESS["AWG_BONUS_PARTNER_URI"] = "http://didyk.pp.ua";

$MESS["AWG_BONUS_INSTALL"] = "Установка модуля \"Бонусная система\"";
$MESS["AWG_BONUS_UNINSTALL"] = "Деинсталяция модуля \"Бонусная система\"";

$MESS["AWG_BONUS_INSTALL_ERROR_VERSION"] = "Версия главного модуля ниже 14. Не поддерживается технология D7. Пожалуйста обновите систему.";

$MESS["AWG_BONUS_IBLOCK_TYPE_NAME_EN"] = "awg.bonus";
$MESS["AWG_BONUS_IBLOCK_TYPE_NAME_RU"] = "Бонусная система";
$MESS["AWG_BONUS_IBLOCK_TYPE_ALREADY_EXISTS"] = "Такой тип инфоблока уже существует";
$MESS["AWG_BONUS_IBLOCK_ALREADY_EXISTS"] = "Инфоблок с таким кодом уже существует";
$MESS["AWG_BONUS_IBLOCK_TYPE_DELETION_ERROR"] = "Ошибка удаления типа инфоблока";
?>