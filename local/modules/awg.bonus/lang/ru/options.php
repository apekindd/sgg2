<?
$MESS["AWG_BONUS_TAB_SETTINGS"] = "Основные настройки";
$MESS["AWG_BONUS_SELECT"] = "Выберите";
$MESS["AWG_BONUS_TAB_NASTROYKI"] = "Настройки";
$MESS["AWG_BONUS_OPTION_BONUS_REGISTR_TITLE"] = "Начисление бонусов при регистрации";
$MESS["AWG_BONUS_OPTION_BONUS_REGISTR_DEFAULT_VALUE"] = "100";
$MESS["AWG_BONUS_OPTION_BONUS_TICKET_TITLE"] = "Процент бонусов за покупку билетов";
$MESS["AWG_BONUS_OPTION_BONUS_TICKET_DEFAULT_VALUE"] = "5";
$MESS["AWG_BONUS_OPTION_BONUS_ANKETA_TITLE"] = "Начисление бонусов при заполнении анкеты";
$MESS["AWG_BONUS_OPTION_BONUS_ANKETA_DEFAULT_VALUE"] = "100";
$MESS["AWG_BONUS_OPTION_BONUS_REF_TITLE"] = "Начисление бонусов за друга";
$MESS["AWG_BONUS_OPTION_BONUS_REF_DEFAULT_VALUE"] = "100";
$MESS["AWG_BONUS_OPTION_COUNT_TICKETS_TITLE"] = "Количество билетов с оплатой бонусами";
$MESS["AWG_BONUS_OPTION_COUNT_TICKETS_DEFAULT_VALUE"] = "50";

$MESS["AWG_BONUS_OPTION_YEARS_TITLE"] = "Годы, через сколько сгорают бонусы";
$MESS["AWG_BONUS_OPTION_YEARS_DEFAULT_VALUE"] = "1";
$MESS["AWG_BONUS_OPTION_MONTHS_TITLE"] = "Месяцы, через сколько сгорают бонусы";
$MESS["AWG_BONUS_OPTION_MONTHS_DEFAULT_VALUE"] = "0";
$MESS["AWG_BONUS_OPTION_DAYS_TITLE"] = "Дни, через сколько сгорают бонусы";
$MESS["AWG_BONUS_OPTION_DAYS_DEFAULT_VALUE"] = "0";

$MESS["AWG_BONUS_OPTION_ENABLE_BONUS_TITLE"] = "Оплата бонусами";
$MESS["AWG_BONUS_OPTION_INFOBLOK_TITLE"] = "Инфоблок c мероприятиями";
$MESS["AWG_BONUS_OPTION_INFOBLOK_ANKETA_TITLE"] = "Инфоблок c анкетами";

?>