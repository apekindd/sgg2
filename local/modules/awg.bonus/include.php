<?
$moduleId = "awg.bonus";

CModule::IncludeModule($moduleId);

/*$arClasses = array(
//    "AWGBonus" => "classes/general/bonus.php",
);

CModule::AddAutoloadClasses($moduleId, $arClasses);
*/

class AWGBonus {
	const KEY = '1234';
	const METHOD = 'aes128';

	public function __construct() {
    }

    public function getBonus($USER_ID) {
	    global $DB;
	    if ($USER_ID > 0) {
		    $mysql_result = $DB->Query('SELECT * FROM awg_bonus_account WHERE ID="'.$USER_ID.'"');
			$result = $mysql_result->GetNext();
			if (isset($result["BONUS"])) return $result["BONUS"];
	    }
	    return false;
    }

    // Проверка на возможность оплаты мероприятия бонусами
    public function checkBonus($EVENT_ID) {
		$arPercent = array();
    	// Проценты из настройки модуля
    	$arPercent[] = array("BONUS_ENABLE" => 1, "BONUS_PERCENT" => COption::GetOptionString("awg.bonus", "bonus_ticket"), "BONUS_COUNT" => COption::GetOptionString("awg.bonus", "count_tickets"));

		// Проценты из категорий мероприятий
		$Element = CIBlockElement::GetByID($EVENT_ID);
		if ($arItem = $Element->GetNext()) {
			$evCat  = $arItem["IBLOCK_SECTION_ID"];
		}
		$resSection = CIBlockSection::GetNavChain(false, $evCat);
		$catID = array();
		while ($arSection = $resSection->GetNext()) {
			$catID[] = $arSection["ID"];
		}
		$arFilter = array('IBLOCK_ID'=>COption::GetOptionString("awg.bonus", "infoblok"), 'ID' => $catID);
		$arSelect = array('ID', 'UF_BONUS_ENABLE', 'UF_BONUS_PERCENT', 'UF_BONUS_COUNT');
		$db_list = CIBlockSection::GetList(Array($by=>$order), $arFilter, false, $arSelect);

		while($ar_result = $db_list->GetNext()) {
			$arPercent[] = array("BONUS_ENABLE" => $ar_result["UF_BONUS_ENABLE"], "BONUS_PERCENT" => $ar_result["UF_BONUS_PERCENT"], "BONUS_COUNT" => $ar_result["UF_BONUS_COUNT"]);
		}

		// Проценты из конкретного мероприятия
    	$arSelect = Array("PROPERTY_BONUS_PERCENT", "PROPERTY_BONUS_ENABLE", "PROPERTY_BONUS_COUNT");
		$arFilter = Array("IBLOCK_ID"=>COption::GetOptionString("awg.bonus", "infoblok"), "ID"=>$EVENT_ID);
		$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>50), $arSelect);
		$ob = $res->GetNextElement();
		$arFields = $ob->GetFields();
		if ($arFields["PROPERTY_BONUS_ENABLE_VALUE"] == "Да") {
			$arFields["PROPERTY_BONUS_ENABLE_VALUE"] = 1;
		} elseif ($arFields["PROPERTY_BONUS_ENABLE_VALUE"] == "Нет") {
			$arFields["PROPERTY_BONUS_ENABLE_VALUE"] = 0;
		}
		$arPercent[] = array("BONUS_ENABLE" => $arFields["PROPERTY_BONUS_ENABLE_VALUE"], "BONUS_PERCENT" => $arFields["PROPERTY_BONUS_PERCENT_VALUE"], "BONUS_COUNT" => $arFields["PROPERTY_BONUS_COUNT_VALUE"]);

		$enable = 0;
		$percent = 0;
		$count = 0;
		foreach ($arPercent as $arItem) {
			if ($arItem["BONUS_PERCENT"] !== NULL) $percent = $arItem["BONUS_PERCENT"];
			if ($arItem["BONUS_COUNT"] !== NULL) $count = $arItem["BONUS_COUNT"];
			if ($arItem["BONUS_ENABLE"] == 1) {
				$enable = 1;
			} elseif ($arItem["BONUS_ENABLE"] == 0) {
				$enable = 0;
			}
		}
		if (($enable == 1) && (COption::GetOptionString("awg.bonus", "enable_bonus") == "Y") && ($count > 0)) {
			return true;
		} else {
			return false;
		}
	}

    /*
    	Изменение бонусов на бонусном счет пользователя и запись в журнал
		Если в таблице еше нет записей для данного пользователя, то сначало создается запись
		$TYPE - тип события:
			1 - Регистрация
			2 - Заполнение анкеты
			3 - Приглашение друга
			4 - Покупка билета
			5 - Списание бонусов при покупке билетов
			6 - Сгорание бонусов
	*/
    public function changeBonus($USER_ID, $TYPE, $TOTAL = NULL, $EVENT_ID, $COUNT = NULL, $ARTICKET) {
	    $USER_ID = intval($USER_ID);
	    $TYPE = intval($TYPE);
	    $TOTAL = intval($TOTAL);
	    $COUNT = intval($COUNT);

	    if (COption::GetOptionString("awg.bonus", "enable_bonus") == "Y") {
		    switch ($TYPE) {
			    case 1:
				    $count = COption::GetOptionString("awg.bonus", "bonus_registr");
			        self::mysql_addBonus($USER_ID, $count, $TYPE);
			        return true;
			    case 2:
				    $count = COption::GetOptionString("awg.bonus", "bonus_anketa");
			        self::mysql_addBonus($USER_ID, $count, $TYPE);
			        return true;
			    case 3:
			        $count = COption::GetOptionString("awg.bonus", "bonus_ref");
			        self::mysql_addBonus($USER_ID, $count, $TYPE);
			        return true;
			    case 4:
			    	if (!intval($TOTAL)) return false;

					$arPercent = array();
			    	// Проценты из настройки модуля
			    	$arPercent[0] = array(
						"BONUS_ENABLE" => (COption::GetOptionString("awg.bonus", "enable_bonus") == "Y")? 1 : 0,
						"BONUS_PERCENT" => (COption::GetOptionString("awg.bonus", "bonus_ticket") > 0) ? COption::GetOptionString("awg.bonus", "bonus_ticket") : 0,
						"BONUS_COUNT" => (COption::GetOptionString("awg.bonus", "count_tickets") > 0) ? COption::GetOptionString("awg.bonus", "count_tickets") : 0
					);

					// Проценты из категорий мероприятий
					$Element = CIBlockElement::GetByID($EVENT_ID);
					if ($arItem = $Element->GetNext()) {
						$evCat  = $arItem["IBLOCK_SECTION_ID"];
					}
					$resSection = CIBlockSection::GetNavChain(false, $evCat);
					$catID = array();
					while ($arSection = $resSection->GetNext()) {
						$catID[] = $arSection["ID"];
					}
					$arFilter = array('IBLOCK_ID'=>COption::GetOptionString("awg.bonus", "infoblok"), 'ID' => $catID);
					$arSelect = array('ID', 'UF_BONUS_ENABLE', 'UF_BONUS_PERCENT', 'UF_BONUS_COUNT');
					$db_list = CIBlockSection::GetList(Array(), $arFilter, false, $arSelect);

					while($ar_result = $db_list->GetNext()) {
						if($ar_result["UF_BONUS_PERCENT"] === NULL || $ar_result["UF_BONUS_COUNT"] == NULL){
							continue;
						}
						$arPercent[0] = array(
						"BONUS_ENABLE" => ($ar_result["UF_BONUS_ENABLE"] == 1)? 1 : 0,
						"BONUS_PERCENT" => ($ar_result["UF_BONUS_PERCENT"] > 0)? $ar_result["UF_BONUS_PERCENT"] : 0,
						"BONUS_COUNT" => ($ar_result["UF_BONUS_COUNT"] > 0)? $ar_result["UF_BONUS_COUNT"] : 0
						);
					}

					// Проценты из конкретного мероприятия
			    	$arSelect = Array("PROPERTY_BONUS_PERCENT", "PROPERTY_BONUS_ENABLE", "PROPERTY_BONUS_COUNT");
					$arFilter = Array("IBLOCK_ID"=>COption::GetOptionString("awg.bonus", "infoblok"), "ID"=>$EVENT_ID);

					$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>50), $arSelect);
					while($ob = $res->GetNextElement())
					{
						$arFields = $ob->GetFields();
						if($arFields["PROPERTY_BONUS_PERCENT_VALUE"] === NULL || $arFields["PROPERTY_BONUS_COUNT_VALUE"] === NULL){
							continue;
						}
						if ($arFields["PROPERTY_BONUS_ENABLE_VALUE"] == "Да") {
							$arFields["PROPERTY_BONUS_ENABLE_VALUE"] = 1;
						} elseif ($arFields["PROPERTY_BONUS_ENABLE_VALUE"] == "Нет") {
							$arFields["PROPERTY_BONUS_ENABLE_VALUE"] = 0;
						}
						$arPercent[0] = array(
							"BONUS_ENABLE" => $arFields["PROPERTY_BONUS_ENABLE_VALUE"],
							"BONUS_PERCENT" => ($arFields["PROPERTY_BONUS_PERCENT_VALUE"] > 0)? $arFields["PROPERTY_BONUS_PERCENT_VALUE"] : 0,
							"BONUS_COUNT" => ($arFields["PROPERTY_BONUS_COUNT_VALUE"] > 0)? $arFields["PROPERTY_BONUS_COUNT_VALUE"] : 0
						);
					}


/*					if ($arFields["PROPERTY_BONUS_ENABLE_VALUE"] == "Да") {
						if ($arFields["PROPERTY_BONUS_PERCENT_VALUE"] !== NULL) {
							$percent = $arFields["PROPERTY_BONUS_PERCENT_VALUE"];
						}
					} else {
						$percent = 0;
					}
*/
					$enable = 0;
					$percent = 0;
					$count = 0;

					foreach ($arPercent as $arItem) {
						if ($arItem["BONUS_PERCENT"] != 0) $percent = $arItem["BONUS_PERCENT"];
						if ($arItem["BONUS_COUNT"] != 0) $count = $arItem["BONUS_COUNT"];
						if ($arItem["BONUS_ENABLE"] == 1) {
							$enable = 1;
						} elseif ($arItem["BONUS_ENABLE"] == 0) {
							$enable = 0;
						}
					}

					$countBonus = self::getCountBonus($EVENT_ID, $count-$COUNT);
					//$countBonus = self::getCountBonus($EVENT_ID, $count);

                    if (isset($ARTICKET)) {
                        global $DB, $USER;
                        $arTickets = array();
                        foreach ($ARTICKET as $key => $tValue) {
                            $dbQuery = $DB->Query('SELECT * FROM awg_bonus_ticket WHERE ID='.intval($tValue["sector_id"]));
                            $ticketExist = $dbQuery->GetNext();
                            //$Disable = $ticketExist["DISABLE"];
                            if ($ticketExist["DISABLE"] == 1) {
                                $ticketExist["DISABLE"] = 0;
                            } else {
                                $ticketExist["DISABLE"] = $enable;
                            };
                            if ($ticketExist["COUNT"] == NULL) {
                                $ticketExist["COUNT"] = $count;
                            };
                            if ($ticketExist["PERCENT"] == NULL) {
                                $ticketExist["PERCENT"] = $percent;
                            };
                            $arTickets[$tValue["sector_id"]] = [
                                "ID" => $tValue["sector_id"],
                                "ENABLE" => $ticketExist["DISABLE"],
                                "PRICE" => $tValue["price"],
                                "COUNT" => $ticketExist["COUNT"],
                                "PERCENT" => $ticketExist["PERCENT"],
                            ];
                        }

                        if ($USER_ID == NULL || $USER_ID == 0 || !(isset($USER_ID))) {
                            $USER_ID = $USER->GetID();
                        }

                        foreach ($arTickets as $key => $value) {
                            //$countBonus = self::getCountBonus($EVENT_ID, $count-$COUNT, $value);
                            $countBonus = self::getCountBonus($EVENT_ID, $count-1, $value);
                            if ($value["ENABLE"] == 1 && $countBonus) {
                                if ($value["PERCENT"] !== 0) {
                                    $count = $value["PRICE"]/100*$value["PERCENT"];
                                    self::mysql_addBonus($USER_ID, $count, $TYPE);
                                }
                                self::mysql_dateOrder($USER_ID);
                            }
                        }
                        return true;
                    }


					if ($enable == 1 && $countBonus) {
						if ($percent !== 0) {
					        $count = $TOTAL/100*$percent;
					        self::mysql_addBonus($USER_ID, $count, $TYPE);
						}
						self::mysql_dateOrder($USER_ID);
						return true;
					} else {
						return false;
					}
				case 5:
			        if (!intval($TOTAL)) return false;
			        // Списание бонусов у пользователя
			        self::mysql_subBonus($USER_ID, $TOTAL, $TYPE);

			        // Списание количеста бонусных билетов

			    	// Количество билетов из настройки модуля
			    	$countTicket = (COption::GetOptionString("awg.bonus", "count_tickets") > 0) ? COption::GetOptionString("awg.bonus", "count_tickets") : 0;

					// Количество билетов из категорий мероприятий
					$Element = CIBlockElement::GetByID($EVENT_ID);
					if ($arItem = $Element->GetNext()) {
						$evCat  = $arItem["IBLOCK_SECTION_ID"];
					}
					$resSection = CIBlockSection::GetNavChain(false, $evCat);
					$catID = array();
					while ($arSection = $resSection->GetNext()) {
						$catID[] = $arSection["ID"];
					}
					$arFilter = array('IBLOCK_ID'=>COption::GetOptionString("awg.bonus", "infoblok"), 'ID' => $catID);
					$arSelect = array('ID', 'UF_BONUS_ENABLE', 'UF_BONUS_PERCENT', 'UF_BONUS_COUNT');
					$db_list = CIBlockSection::GetList(Array(), $arFilter, false, $arSelect);

					while($ar_result = $db_list->GetNext()) {
						if (intval($ar_result["UF_BONUS_COUNT"]) > 0) {
							$countTicket = intval($ar_result["UF_BONUS_COUNT"]);
						}
					}

					// Количество билетов из конкретного мероприятия
			    	$arSelect = Array("PROPERTY_BONUS_PERCENT", "PROPERTY_BONUS_ENABLE", "PROPERTY_BONUS_COUNT");
					$arFilter = Array("IBLOCK_ID"=>COption::GetOptionString("awg.bonus", "infoblok"), "ID"=>$EVENT_ID);

					$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>50), $arSelect);
					while($ob = $res->GetNextElement())
					{
						$arFields = $ob->GetFields();
						if (intval($arFields["PROPERTY_BONUS_COUNT_VALUE"]) > 0) {
							$countTicket = intval($arFields["PROPERTY_BONUS_COUNT_VALUE"]);
						}
					}

					// Количество билетов из типа билета
                    global $DB, $USER;
                    $arTickets = array();
                    foreach ($GLOBALS["AWG_TICKET"] as $key => $tValue) {
                        $dbQuery = $DB->Query('SELECT * FROM awg_bonus_ticket WHERE ID='.intval($tValue["sector_id"]));
                        $ticketExist = $dbQuery->GetNext();
                        if ($ticketExist["COUNT"] == NULL) {
                            $ticketExist["COUNT"] = $countTicket;
                        };
                        $arTickets[$tValue["sector_id"]] = [
                            "ID" => $tValue["sector_id"],
                            "PRICE" => $tValue["price"],
                            "COUNT" => $ticketExist["COUNT"],
                        ];
                        self::mysql_subTicket($tValue["sector_id"], $ticketExist["COUNT"] - 1);
                    }
			        return true;
			    case 6:
			        if (!intval($TOTAL)) return false;
			        self::mysql_subBonus($USER_ID, $TOTAL, $TYPE);
			        return true;
			}
	    }
		return false;
    }

	public function dateCheckOrder() {
		global $DB;
		$dbQuery = $DB->Query('SELECT * FROM b_option WHERE MODULE_ID = "awg.bonus"');
		$arSetting = array();
		while ($row = $dbQuery->Fetch()) {
			$arSetting[$row["NAME"]] = $row["VALUE"];
		}
		$date = new DateTime();
		$date->modify('-'.$arSetting["years"].' year');
		$date->modify('-'.$arSetting["months"].' month');
		$date->modify('-'.$arSetting["days"].' day');

		$dbQuery = $DB->Query('SELECT * FROM awg_bonus_account WHERE DATA < "'.$date->format('Y-m-d').'"');
		while ($row = $dbQuery->Fetch()) {
			self::changeBonus($row["ID"], 6, $row["BONUS"]);
		}

//		mail('avp1975@gmail.com', 'Агент', 'Агент123');
		return "awgBonus::dateCheckOrder();";
	}

	public function addCountBonus($EVENT_ID, $COUNT) {
	    global $DB;
	    $dbQuery = $DB->Query('SELECT * FROM awg_bonus_count WHERE ID='.intval($EVENT_ID));
	    $userExist = $dbQuery->GetNext();
	    if (!$userExist) {
		    $dbQuery = $DB->Query('INSERT INTO `awg_bonus_count` (`ID`, `COUNT`) VALUES ('.intval($EVENT_ID).', '.$COUNT.')');
	    } else {
		    $dbQuery = $DB->Query('UPDATE `awg_bonus_count` SET `COUNT` = `COUNT`+'.$COUNT.' WHERE `ID` = '.intval($EVENT_ID));
	    }
    }

    public function getCountBonus($EVENT_ID, $COUNT, $TICKET) {
	    global $DB;
        $dbQuery = $DB->Query('SELECT * FROM awg_bonus_count WHERE ID='.intval($EVENT_ID));
        $countBonus = $dbQuery->GetNext();
        $return = true;
        if ($countBonus) {
            if ($countBonus["COUNT"] <= $COUNT) {
                $return = true;
            } else {
                $return = false;
            }
        }
	    if (isset($TICKET)) {
            $dbQuery = $DB->Query('SELECT * FROM awg_bonus_ticket WHERE ID='.intval($TICKET["ID"]));
            $curTicket = $dbQuery->GetNext();
            if ($curTicket) {
                if ($curTicket["COUNT"] !== NULL) {
                    if ($curTicket["COUNT"] <= $COUNT) {
                        $return = true;
                    } else {
                        $return = false;
                    }
                }
            }
        }
/*	    $dbQuery = $DB->Query('SELECT * FROM awg_bonus_count WHERE ID='.intval($EVENT_ID));
	    $countBonus = $dbQuery->GetNext();
	    if ($countBonus) {
		    if ($countBonus["COUNT"] <= $COUNT) {
				return true;
		    } else {
			    return false;
		    }
	    }
		return true; */
        return $return;
    }

	public function mysql_dateOrder($USER_ID) {
		global $DB;
		$dbQuery = $DB->Query('UPDATE `awg_bonus_account` SET `DATA` = now() WHERE `ID` = '.intval($USER_ID));
	}

    public function mysql_addBonus($USER_ID, $BONUS, $TYPE) {
	    global $DB;
	    $dbQuery = $DB->Query('SELECT * FROM awg_bonus_account WHERE ID='.intval($USER_ID));
	    $userExist = $dbQuery->GetNext();
	    if (!$userExist) {
		    $dbQuery = $DB->Query('INSERT INTO `awg_bonus_account` (`ID`, `BONUS`) VALUES ('.intval($USER_ID).', '.$BONUS.')');
	    } else {
		    $dbQuery = $DB->Query('UPDATE `awg_bonus_account` SET `BONUS` = `BONUS`+'.$BONUS.' WHERE `ID` = '.intval($USER_ID));
	    }
	    $dbQuery = $DB->Query('INSERT INTO `awg_bonus_log` (`USER_ID`, `TYPE`, `BONUS`) VALUES ('.intval($USER_ID).', '.intval($TYPE).', '.$BONUS.')');
    }

    public function mysql_subBonus($USER_ID, $BONUS, $TYPE) {
	    global $DB;
		$dbQuery = $DB->Query('UPDATE `awg_bonus_account` SET `BONUS` = `BONUS`-'.$BONUS.' WHERE `ID` = '.intval($USER_ID));
	    $dbQuery = $DB->Query('INSERT INTO `awg_bonus_log` (`USER_ID`, `TYPE`, `BONUS`) VALUES ('.intval($USER_ID).', '.intval($TYPE).', '.$BONUS.')');
    }

    public function mysql_subTicket($ID, $COUNT) {
	    global $DB;
		$dbQuery = $DB->Query('SELECT * FROM awg_bonus_ticket WHERE ID='.intval($ID));
	    $IDExist = $dbQuery->GetNext();
	    if (!$IDExist) {
		    $dbQuery = $DB->Query('INSERT INTO `awg_bonus_ticket` (`ID`, `COUNT`) VALUES ('.intval($ID).', '.$COUNT.')');
	    } else {
		    $dbQuery = $DB->Query('UPDATE `awg_bonus_ticket` SET `COUNT` = '.$COUNT.' WHERE `ID` = '.intval($ID));
	    }
    }

/*
	// Получение реферальной ссылки по ID пользователя
	public function getRefLink($USER_ID) {
		return openssl_encrypt ($USER_ID, self::METHOD, self::KEY);
    }

    // Получение ID пользователя по реферальной ссылке
	public function getRefUser($LINK) {
		return openssl_decrypt ($LINK, self::METHOD, self::KEY);
    }
*/
    // Получение реферальной ссылки по ID пользователя
    public function getRefLink($unencoded, $key = self::KEY) { //Шифруем
		$string = base64_encode($unencoded); //Переводим в base64

		$arr = array(); //Это массив
		$x = 0;
		while ($x++< strlen($string)) { //Цикл
			$arr[$x-1] = md5(md5($key.$string[$x-1]).$key); //Почти чистый md5
			$newstr = $newstr.$arr[$x-1][3].$arr[$x-1][6].$arr[$x-1][1].$arr[$x-1][2]; //Склеиваем символы
		}
		return $newstr; // Возвращаем строку
	}

	// Получение ID пользователя по реферальной ссылке
	public function getRefUser($encoded, $key = self::KEY) { //расшифровываем
		$strofsym = "qwertyuiopasdfghjklzxcvbnm1234567890QWERTYUIOPASDFGHJKLZXCVBNM="; //Символы, с которых состоит base64-ключ
		$x = 0;
		while ($x++<= strlen($strofsym)) { //Цикл
			$tmp = md5(md5($key.$strofsym[$x-1]).$key); //Хеш, который соответствует символу, на который его заменят.
			$encoded = str_replace($tmp[3].$tmp[6].$tmp[1].$tmp[2], $strofsym[$x-1], $encoded); //Заменяем №3,6,1,2 из хеша на символ
		}
		return base64_decode($encoded); //Возвращаем расшифрованную строку
	}

	public function OnBeforeProlog() {
		if (isset($_REQUEST["ref"])) {
			setcookie("REFERRAL", $_REQUEST["ref"], time()+31556926);
		}

        if ($_SERVER['REQUEST_METHOD']=='POST' && $GLOBALS['APPLICATION']->GetCurPage()=='/bitrix/admin/iblock_element_edit.php' && $_REQUEST['IBLOCK_ID']==4)
        {
            global $DB;
            foreach ($_POST["BONUS"] as $key => $value) {
                $dbQuery = $DB->Query('SELECT * FROM awg_bonus_ticket WHERE ID='.intval($key));
                $ticketExist = $dbQuery->GetNext();
                if (isset($value["DISABLE"]))
                    $Disable = 1;
                else
                    $Disable = NULL;

                if (!$ticketExist) {
                    $dbQuery = $DB->Query('INSERT INTO `awg_bonus_ticket` (`ID`, `ID_EVENT_B`, `ID_EVENT_T`, `DISABLE`, `COUNT`, `PERCENT`) VALUES ('.intval($key).', NULL, NULL, '.($Disable == 1 ? 1 : "NULL").', '.(is_numeric($value["COUNT"]) ? $value["COUNT"] : "NULL").', '.($value["PERCENT"] > 0 ? $value["PERCENT"] : "NULL").')');
                } else {
                    $dbQuery = $DB->Query('UPDATE `awg_bonus_ticket` SET `DISABLE` = '.($Disable == 1 ? 1 : "NULL").', `COUNT` = '.(is_numeric($value["COUNT"]) ? $value["COUNT"] : "NULL").', `PERCENT` = '.($value["PERCENT"] > 0 ? $value["PERCENT"] : "NULL").' WHERE `ID` = '.intval($key));
                }
            }
        }
	}

	public function OnAfterUserAdd(&$arFields) {
		// Бонусы за регистрацию
        if ($arFields["RESULT"] == true) {
            $user_id = $arFields["ID"];
            $addBonus = awgBonus::changeBonus($user_id, 1);
	        $user_password = $_REQUEST["USER_PASSWORD"];

            if (isset($_COOKIE["REFERRAL"])) {
                // Бонусы за реферала
                $ref_user_id = awgBonus::getRefUser($_COOKIE["REFERRAL"]);
                $addBonus = awgBonus::changeBonus($ref_user_id, 3);
            }
	        if(!$user_password) {
		        $user_password = $arFields["USER_PASSWORD"];
	        }
            $link = 'https://' . \Codepilots\Config::getSubdomain() . 'showgogo.ru/login/?auth=' . urlencode(\Codepilots\Helper::getCodeFromId($user_id));
            $params = [
                'login' => $arFields["EMAIL"],
                'pw' => $user_password,
                'email' => $arFields["EMAIL"],
                'link' => $link
            ];
            $params = http_build_query($params);
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_URL, "http://ts-reestr.showgogo.ru/mail.php");
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
            $data = curl_exec($ch);
        }
	}

    public function OnAdminTabControlBegin(&$form) {
        if($GLOBALS["APPLICATION"]->GetCurPage() == "/bitrix/admin/iblock_element_edit.php" && $_REQUEST['IBLOCK_ID'] == 4) {

            $arSelect = Array("PROPERTY_TICKET_SYSTEM_IDS");
            $arFilter = Array("IBLOCK_ID"=>IntVal($_REQUEST['IBLOCK_ID']), "ID"=>IntVal($_REQUEST['ID']));
            $res = CIBlockElement::GetList(Array(), $arFilter, false, Array(), $arSelect);
            $ob = $res->GetNextElement();
            $arFields = $ob->GetFields();
            $tmpID = $arFields["PROPERTY_TICKET_SYSTEM_IDS_VALUE"];

            require $_SERVER["DOCUMENT_ROOT"].'/widget/config/config.php';
            spl_autoload_register(function($class) {
                if (file_exists($_SERVER["DOCUMENT_ROOT"]."/widget/classes/{$class}.class.php"))
                    require_once $_SERVER["DOCUMENT_ROOT"]."/widget/classes/{$class}.class.php";
            });

            $Showgogo = new Showgogo();
            //$result = $Showgogo->getEventData(943); //идентификатор в билетной системе
            $result = $Showgogo->getEventData($tmpID); //идентификатор в билетной системе

            $arParamTickets = array();
            foreach ($result["data"] as $key => $value) {
                $arParamTickets[$value["sector_id"]] = array(
                    "NAME" => $value["sector_name"],
                    "DISABLE" => NULL,
                    "PERCENT" => NULL,
                    "COUNT" => NULL
                );
            }

            global $DB;
            $sel = implode(", ", array_keys($arParamTickets));
            if (strlen($sel) > 0) {
                $dbQuery = $DB->Query('SELECT * FROM awg_bonus_ticket WHERE ID in ('.$sel.')');
                while ($row = $dbQuery->Fetch()) {
                    $arParamTickets[$row["ID"]]["DISABLE"] = $row["DISABLE"];
                    $arParamTickets[$row["ID"]]["PERCENT"] = $row["PERCENT"];
                    $arParamTickets[$row["ID"]]["COUNT"] = $row["COUNT"];
                }
            }

            $insStr = '';
            foreach ($arParamTickets as $key => $value) {
                $insStr .= '<tr class="heading" id="tr_TICKET_'.$key.'"><td colspan="2">'.$value["NAME"].'</td></tr>';
                $insStr .= '<tr id="tr_DISABLE_'.$value["sector_id"].'"><td class="adm-detail-content-cell-l" width="40%">Не участвует в бонусной программе:</td><td class="adm-detail-content-cell-r"><input name="BONUS['.$key.'][DISABLE]" '.($value["DISABLE"] == 1 ? 'checked="checked"' : '').' value="" class="111adm-designed-checkbox" type="checkbox"></td></tr>';
                $insStr .= '<tr id="tr_COUNT_'.$value["sector_id"].'"><td class="adm-detail-valign-top adm-detail-content-cell-l" width="40%">Количество билетов:</td><td class="adm-detail-content-cell-r" width="60%"><table class="nopadding" width="100%" border="0" cellspacing="0" cellpadding="0"><tbody><tr><td><input name="BONUS['.$key.'][COUNT]" value="'.$value["COUNT"].'" size="30" type="text"><br></td></tr></tbody></table></td></tr>';
                $insStr .= '<tr id="tr_PERCENT_'.$value["sector_id"].'"><td class="adm-detail-valign-top adm-detail-content-cell-l" width="40%">Процент бонусов:</td><td class="adm-detail-content-cell-r" width="60%"><table class="nopadding" width="100%" border="0" cellspacing="0" cellpadding="0"><tbody><tr><td><input name="BONUS['.$key.'][PERCENT]" value="'.$value["PERCENT"].'" size="30" type="text"><br></td></tr></tbody></table></td></tr>';
            }

            CJSCore::Init(array("jquery"));
            ob_start();?>

            <script type="text/javascript">
                var HTMLString = '<tr class="heading" id="tr_IBLOCK_ELEMENT_PROP_VALUE_awg"><td colspan="2">КАТЕГОРИИ БИЛЕТОВ:</td></tr>';
                HTMLString += '<?=$insStr;?>';

                $(function() {
                    var insert= '';
                    $("#tr_PROPERTY_86").after(HTMLString);
                });
            </script>

            <?
            $sContent = ob_get_clean();
            $GLOBALS['APPLICATION']->AddHeadString($sContent);
        }
    }

	/* old form
	public function OnAfterIBlockElementAdd(&$arFields) {
		if ($arFields["IBLOCK_ID"] == COption::GetOptionString("awg.bonus", "infoblok_anketa")) {
			$user_id = $arFields["PROPERTY_VALUES"]["USER_ID"];
			$addBonus = awgBonus::changeBonus($user_id, 2);
		}
	}
	*/
}
?>