<?
class AWGBonus {
	const KEY = '1234';
	const METHOD = 'aes128';

	public function __construct() {
    }
    
    public function getBonus($USER_ID) {
	    global $DB;
	    if ($USER_ID > 0) {
		    $mysql_result = $DB->Query('SELECT * FROM awg_bonus_account WHERE ID="'.$USER_ID.'"');
			$result = $mysql_result->GetNext();
			if (isset($result["BONUS"])) return $result["BONUS"];		    
	    }
	    return false;
    }
    
    // Проверка на возможность оплаты мероприятия бонусами
    public function checkBonus($EVENT_ID) {
		$arPercent = array();			    	
    	// Проценты из настройки модуля
    	$arPercent[] = array("BONUS_ENABLE" => 1, "BONUS_PERCENT" => COption::GetOptionString("awg.bonus", "bonus_ticket"), "BONUS_COUNT" => COption::GetOptionString("awg.bonus", "count_tickets"));

		// Проценты из категорий мероприятий
		$Element = CIBlockElement::GetByID($EVENT_ID);
		if ($arItem = $Element->GetNext()) {
			$evCat  = $arItem["IBLOCK_SECTION_ID"];
		}
		$resSection = CIBlockSection::GetNavChain(false, $evCat);
		$catID = array();
		while ($arSection = $resSection->GetNext()) {
			$catID[] = $arSection["ID"];
		}
		$arFilter = array('IBLOCK_ID'=>COption::GetOptionString("awg.bonus", "infoblok"), 'ID' => $catID);
		$arSelect = array('ID', 'UF_BONUS_ENABLE', 'UF_BONUS_PERCENT', 'UF_BONUS_COUNT');
		$db_list = CIBlockSection::GetList(Array($by=>$order), $arFilter, false, $arSelect);

		while($ar_result = $db_list->GetNext()) {
			$arPercent[] = array("BONUS_ENABLE" => $ar_result["UF_BONUS_ENABLE"], "BONUS_PERCENT" => $ar_result["UF_BONUS_PERCENT"], "BONUS_COUNT" => $ar_result["UF_BONUS_COUNT"]);
		}

		// Проценты из конкретного мероприятия
    	$arSelect = Array("PROPERTY_BONUS_PERCENT", "PROPERTY_BONUS_ENABLE", "PROPERTY_BONUS_COUNT");
		$arFilter = Array("IBLOCK_ID"=>COption::GetOptionString("awg.bonus", "infoblok"), "ID"=>$EVENT_ID);
		$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>50), $arSelect);
		$ob = $res->GetNextElement();
		$arFields = $ob->GetFields();
		if ($arFields["PROPERTY_BONUS_ENABLE_VALUE"] == "Да") {
			$arFields["PROPERTY_BONUS_ENABLE_VALUE"] = 1;
		}
		$arPercent[] = array("BONUS_ENABLE" => $arFields["PROPERTY_BONUS_ENABLE_VALUE"], "BONUS_PERCENT" => $arFields["PROPERTY_BONUS_PERCENT_VALUE"], "BONUS_COUNT" => $arFields["PROPERTY_BONUS_COUNT_VALUE"]);
		
		$enable = 0;
		$percent = 0;
		$count = 0;
		foreach ($arPercent as $arItem) {
			if ($arItem["BONUS_PERCENT"] !== NULL) $percent = $arItem["BONUS_PERCENT"];
			if ($arItem["BONUS_COUNT"] !== NULL) $count = $arItem["BONUS_COUNT"];
			if ($arItem["BONUS_ENABLE"] == 1) {
				$enable = 1;
			} else {
				$enable = 0;
			}
		}
		if (($enable == 1) && (COption::GetOptionString("awg.bonus", "enable_bonus") == "Y") && ($count > 0)) {
			return true;												
		} else {
			return false;
		}
	}
	
    /*
    	Изменение бонусов на бонусном счет пользователя и запись в журнал
		Если в таблице еше нет записей для данного пользователя, то сначало создается запись
		$TYPE - тип события:
			1 - Регистрация
			2 - Заполнение анкеты
			3 - Приглашение друга
			4 - Покупка билета
			5 - Списание бонусов при покупке билетов
			6 - Сгорание бонусов
	*/   
    public function changeBonus($USER_ID, $TYPE, $TOTAL = NULL, $EVENT_ID, $COUNT = NULL) {
	    $USER_ID = intval($USER_ID);
	    $TYPE = intval($TYPE);
	    $TOTAL = intval($TOTAL);
	    $COUNT = intval($COUNT);
	    if (COption::GetOptionString("awg.bonus", "enable_bonus") == "Y") {
		    switch ($TYPE) {
			    case 1:
				    $count = COption::GetOptionString("awg.bonus", "bonus_registr");
			        self::mysql_addBonus($USER_ID, $count, $TYPE);
			        return true;
			    case 2:
				    $count = COption::GetOptionString("awg.bonus", "bonus_anketa");
			        self::mysql_addBonus($USER_ID, $count, $TYPE);
			        return true;
			    case 3:
			        $count = COption::GetOptionString("awg.bonus", "bonus_ref");
			        self::mysql_addBonus($USER_ID, $count, $TYPE);
			        return true;
			    case 4:
			    	if (!intval($TOTAL)) return false;

					$arPercent = array();			    	
			    	// Проценты из настройки модуля
			    	$arPercent[] = array("BONUS_ENABLE" => 1, "BONUS_PERCENT" => COption::GetOptionString("awg.bonus", "bonus_ticket"), "BONUS_COUNT" => COption::GetOptionString("awg.bonus", "count_tickets"));

					// Проценты из категорий мероприятий
					$Element = CIBlockElement::GetByID($EVENT_ID);
					if ($arItem = $Element->GetNext()) {
						$evCat  = $arItem["IBLOCK_SECTION_ID"];
					}
					$resSection = CIBlockSection::GetNavChain(false, $evCat);
					$catID = array();
					while ($arSection = $resSection->GetNext()) {
						$catID[] = $arSection["ID"];
					}
					$arFilter = array('IBLOCK_ID'=>COption::GetOptionString("awg.bonus", "infoblok"), 'ID' => $catID);
					$arSelect = array('ID', 'UF_BONUS_ENABLE', 'UF_BONUS_PERCENT', 'UF_BONUS_COUNT');
					$db_list = CIBlockSection::GetList(Array($by=>$order), $arFilter, false, $arSelect);

					while($ar_result = $db_list->GetNext()) {
						$arPercent[] = array("BONUS_ENABLE" => $ar_result["UF_BONUS_ENABLE"], "BONUS_PERCENT" => $ar_result["UF_BONUS_PERCENT"], "BONUS_COUNT" => $ar_result["UF_BONUS_COUNT"]);
					}					

					// Проценты из конкретного мероприятия
			    	$arSelect = Array("PROPERTY_BONUS_PERCENT", "PROPERTY_BONUS_ENABLE", "PROPERTY_BONUS_COUNT");
					$arFilter = Array("IBLOCK_ID"=>COption::GetOptionString("awg.bonus", "infoblok"), "ID"=>$EVENT_ID);
					$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>50), $arSelect);
					$ob = $res->GetNextElement();
					$arFields = $ob->GetFields();
					if ($arFields["PROPERTY_BONUS_ENABLE_VALUE"] == "Да") {
						$arFields["PROPERTY_BONUS_ENABLE_VALUE"] = 1;
					}
					$arPercent[] = array("BONUS_ENABLE" => $arFields["PROPERTY_BONUS_ENABLE_VALUE"], "BONUS_PERCENT" => $arFields["PROPERTY_BONUS_PERCENT_VALUE"], "BONUS_COUNT" => $arFields["PROPERTY_BONUS_COUNT_VALUE"]);
/*					if ($arFields["PROPERTY_BONUS_ENABLE_VALUE"] == "Да") {
						if ($arFields["PROPERTY_BONUS_PERCENT_VALUE"] !== NULL) {
							$percent = $arFields["PROPERTY_BONUS_PERCENT_VALUE"];
						}
					} else {
						$percent = 0;
					}
*/
					$enable = 0;
					$percent = 0;
					$count = 0;
					foreach ($arPercent as $arItem) {
						if ($arItem["BONUS_PERCENT"] !== NULL) $percent = $arItem["BONUS_PERCENT"];
						if ($arItem["BONUS_COUNT"] !== NULL) $count = $arItem["BONUS_COUNT"];
						if ($arItem["BONUS_ENABLE"] == 1) {
							$enable = 1;
						} else {
							$enable = 0;
						}
					}
					
					$countBonus = self::getCountBonus($EVENT_ID, $count-$COUNT);
					
					if ($enable == 1 && $countBonus) {
						if ($percent !== 0) {
					        $count = intval($TOTAL/100*$percent);
					        self::mysql_addBonus($USER_ID, $count, $TYPE);
						}
						self::mysql_dateOrder($USER_ID);
						return true;												
					} else {
						return false;
					}
				case 5:
			        if (!intval($TOTAL)) return false;
			        self::mysql_subBonus($USER_ID, $TOTAL, $TYPE);
			        return true;
			    case 6:
			        if (!intval($TOTAL)) return false;
			        self::mysql_subBonus($USER_ID, $TOTAL, $TYPE);
			        return true;
			}		    
	    }
		return false;
    }

	public function dateCheckOrder() {
		global $DB;
		$dbQuery = $DB->Query('SELECT * FROM b_option WHERE MODULE_ID = "awg.bonus"');
		$arSetting = array();
		while ($row = $dbQuery->Fetch()) {
			$arSetting[$row["NAME"]] = $row["VALUE"];
		}
		$date = new DateTime();
		$date->modify('-'.$arSetting["years"].' year');
		$date->modify('-'.$arSetting["months"].' month');
		$date->modify('-'.$arSetting["days"].' day');

		$dbQuery = $DB->Query('SELECT * FROM awg_bonus_account WHERE DATA < "'.$date->format('Y-m-d').'"');
		while ($row = $dbQuery->Fetch()) {
			self::changeBonus($row["ID"], 6, $row["BONUS"]);
		}

//		mail('avp1975@gmail.com', 'Агент', 'Агент123');
		return "awgBonus::dateCheckOrder();";
	}
	
	public function addCountBonus($EVENT_ID, $COUNT) {
	    global $DB;
	    $dbQuery = $DB->Query('SELECT * FROM awg_bonus_count WHERE ID='.intval($EVENT_ID));
	    $userExist = $dbQuery->GetNext();
	    if (!$userExist) {
		    $dbQuery = $DB->Query('INSERT INTO `awg_bonus_count` (`ID`, `COUNT`) VALUES ('.intval($EVENT_ID).', '.intval($COUNT).')');
	    } else {
		    $dbQuery = $DB->Query('UPDATE `awg_bonus_count` SET `COUNT` = `COUNT`+'.intval($COUNT).' WHERE `ID` = '.intval($EVENT_ID));
	    }
    }
    
    public function getCountBonus($EVENT_ID, $COUNT) {
	    global $DB;
	    $dbQuery = $DB->Query('SELECT * FROM awg_bonus_count WHERE ID='.intval($EVENT_ID));
	    $countBonus = $dbQuery->GetNext();
	    if ($countBonus) {
		    if ($countBonus["COUNT"] <= $COUNT) {
				return true;
		    } else {
			    return false;
		    }
	    }
		return true;
    }
    
	public function mysql_dateOrder($USER_ID) {
		global $DB;
		$dbQuery = $DB->Query('UPDATE `awg_bonus_account` SET `DATA` = now() WHERE `ID` = '.intval($USER_ID));
	}
	
    public function mysql_addBonus($USER_ID, $BONUS, $TYPE) {
	    global $DB;
	    $dbQuery = $DB->Query('SELECT * FROM awg_bonus_account WHERE ID='.intval($USER_ID));
	    $userExist = $dbQuery->GetNext();
	    if (!$userExist) {
		    $dbQuery = $DB->Query('INSERT INTO `awg_bonus_account` (`ID`, `BONUS`) VALUES ('.intval($USER_ID).', '.intval($BONUS).')');
	    } else {
		    $dbQuery = $DB->Query('UPDATE `awg_bonus_account` SET `BONUS` = `BONUS`+'.intval($BONUS).' WHERE `ID` = '.intval($USER_ID));
	    }
	    $dbQuery = $DB->Query('INSERT INTO `awg_bonus_log` (`USER_ID`, `TYPE`, `BONUS`) VALUES ('.intval($USER_ID).', '.intval($TYPE).', '.intval($BONUS).')');
    }
    
    public function mysql_subBonus($USER_ID, $BONUS, $TYPE) {
	    global $DB;
		$dbQuery = $DB->Query('UPDATE `awg_bonus_account` SET `BONUS` = `BONUS`-'.intval($BONUS).' WHERE `ID` = '.intval($USER_ID));
	    $dbQuery = $DB->Query('INSERT INTO `awg_bonus_log` (`USER_ID`, `TYPE`, `BONUS`) VALUES ('.intval($USER_ID).', '.intval($TYPE).', '.intval($BONUS).')');
    }
/*
	// Получение реферальной ссылки по ID пользователя
	public function getRefLink($USER_ID) {
		return openssl_encrypt ($USER_ID, self::METHOD, self::KEY);
    }
    
    // Получение ID пользователя по реферальной ссылке
	public function getRefUser($LINK) {
		return openssl_decrypt ($LINK, self::METHOD, self::KEY);
    }
*/    
    // Получение реферальной ссылки по ID пользователя
    public function getRefLink($unencoded, $key = self::KEY) { //Шифруем
		$string = base64_encode($unencoded); //Переводим в base64

		$arr = array(); //Это массив
		$x = 0;
		while ($x++< strlen($string)) { //Цикл
			$arr[$x-1] = md5(md5($key.$string[$x-1]).$key); //Почти чистый md5
			$newstr = $newstr.$arr[$x-1][3].$arr[$x-1][6].$arr[$x-1][1].$arr[$x-1][2]; //Склеиваем символы
		}
		return $newstr; // Возвращаем строку
	}

	// Получение ID пользователя по реферальной ссылке
	public function getRefUser($encoded, $key = self::KEY) { //расшифровываем
		$strofsym = "qwertyuiopasdfghjklzxcvbnm1234567890QWERTYUIOPASDFGHJKLZXCVBNM="; //Символы, с которых состоит base64-ключ
		$x = 0;
		while ($x++<= strlen($strofsym)) { //Цикл
			$tmp = md5(md5($key.$strofsym[$x-1]).$key); //Хеш, который соответствует символу, на который его заменят.
			$encoded = str_replace($tmp[3].$tmp[6].$tmp[1].$tmp[2], $strofsym[$x-1], $encoded); //Заменяем №3,6,1,2 из хеша на символ
		}
		return base64_decode($encoded); //Возвращаем расшифрованную строку
	}
}	
?>