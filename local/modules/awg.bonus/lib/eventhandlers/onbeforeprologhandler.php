<?
namespace Awg\Bonus\EventHandlers;

use Bitrix\Main\Localization;
Localization\Loc::loadMessages(__FILE__);

class OnBeforePrologHandler{
	static public function handler(){
		if (isset($_REQUEST["ref"])) {
			setcookie("REFERRAL", $_REQUEST["ref"]);
		}
	}
}