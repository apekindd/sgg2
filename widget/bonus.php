﻿<?require_once($_SERVER['DOCUMENT_ROOT']. "/bitrix/modules/main/include/prolog_before.php");

if (CModule::IncludeModule("awg.bonus")) {
	$GLOBALS["BONUS"] = array();
	$arSelect = Array("ID", "NAME", "PROPERTY_TICKET_SYSTEM_IDS");
	$arFilter = Array("IBLOCK_ID" => 4, "PROPERTY_TICKET_SYSTEM_IDS" => $request["event_id"]);
	$res = CIBlockElement::GetList(Array(), $arFilter, false, Array(), $arSelect);
	$arResult = array();
	while($ar_fields = $res->GetNext())
	{
		$arResult = $ar_fields;
	}
	if (checkBonus($arResult["ID"], $data["ticket_quantity"])) {
		global $USER;
		$user_id = $USER->getID();
		$userBonus = awgBonus::getBonus($USER->getID());
		if ($userBonus >= $price_summary) {
			$GLOBALS["BONUS"]["may_bonus"] = $userBonus;
		}
	}

	$ticketParams = json_decode($request["tickets"]);
    global $DB;
	foreach ($ticketParams as $key => $value) {
        $dbQuery = $DB->Query('SELECT * FROM awg_bonus_ticket WHERE ID='.intval($key));
        $ticketExist = $dbQuery->GetNext();
        $Disable = $ticketExist["DISABLE"];
        $Count = $ticketExist["COUNT"];

        if (intval($value->count) > 0) {
            if ($Disable == 1 || (isset($Count) && $Count < intval($value->count))) {
                $GLOBALS["BONUS"]["may_bonus"] = 0;
            }
		}
	}
}

function checkBonus($EVENT_ID, $COUNT) {

					$arPercent = array();
			    	// Проценты из настройки модуля
			    	$arPercent[] = array("BONUS_ENABLE" => 1, "BONUS_PERCENT" => COption::GetOptionString("awg.bonus", "bonus_ticket"), "BONUS_COUNT" => COption::GetOptionString("awg.bonus", "count_tickets"));

					// Проценты из категорий мероприятий
					$Element = CIBlockElement::GetByID($EVENT_ID);
					if ($arItem = $Element->GetNext()) {
						$evCat  = $arItem["IBLOCK_SECTION_ID"];
					}
					$resSection = CIBlockSection::GetNavChain(false, $evCat);
					$catID = array();
					while ($arSection = $resSection->GetNext()) {
						$catID[] = $arSection["ID"];
					}
					$arFilter = array('IBLOCK_ID'=>COption::GetOptionString("awg.bonus", "infoblok"), 'ID' => $catID);
					$arSelect = array('ID', 'UF_BONUS_ENABLE', 'UF_BONUS_PERCENT', 'UF_BONUS_COUNT');
					$db_list = CIBlockSection::GetList(Array($by=>$order), $arFilter, false, $arSelect);

					while($ar_result = $db_list->GetNext()) {
						$arPercent[] = array("BONUS_ENABLE" => $ar_result["UF_BONUS_ENABLE"], "BONUS_PERCENT" => $ar_result["UF_BONUS_PERCENT"], "BONUS_COUNT" => $ar_result["UF_BONUS_COUNT"]);
					}

					// Проценты из конкретного мероприятия
			    	$arSelect = Array("PROPERTY_BONUS_PERCENT", "PROPERTY_BONUS_ENABLE", "PROPERTY_BONUS_COUNT");
					$arFilter = Array("IBLOCK_ID"=>COption::GetOptionString("awg.bonus", "infoblok"), "ID"=>$EVENT_ID);
					$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>50), $arSelect);
					$ob = $res->GetNextElement();
					$arFields = $ob->GetFields();
					if ($arFields["PROPERTY_BONUS_ENABLE_VALUE"] == "Да") {
						$arFields["PROPERTY_BONUS_ENABLE_VALUE"] = 1;
					}
					$arPercent[] = array("BONUS_ENABLE" => $arFields["PROPERTY_BONUS_ENABLE_VALUE"], "BONUS_PERCENT" => $arFields["PROPERTY_BONUS_PERCENT_VALUE"], "BONUS_COUNT" => $arFields["PROPERTY_BONUS_COUNT_VALUE"]);

					$enable = 0;
					$percent = 0;
					$count = 0;
					foreach ($arPercent as $arItem) {
						if ($arItem["BONUS_PERCENT"] !== NULL) $percent = $arItem["BONUS_PERCENT"];
						if ($arItem["BONUS_COUNT"] !== NULL) $count = $arItem["BONUS_COUNT"];
						if ($arItem["BONUS_ENABLE"] == 1) {
							$enable = 1;
						} else {
							$enable = 0;
						}
					}

					$countBonus = awgBonus::getCountBonus($EVENT_ID, $count-$COUNT);
					if ($enable == 1 && $countBonus && ($count - $COUNT >= 0) && COption::GetOptionString("awg.bonus", "enable_bonus") == "Y") {
						return true;
					} else {
						return false;
					}
}
?>
