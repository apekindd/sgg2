<?php

class Showgogo {
    public $provider;
    public static $widget_url = "/widget/";
    private static $smarty_instance;
    private static $db_instance;
    private $event_id;
    public $view;
    private $monthes = [
        '01' => 'января',
        '02' => 'февраля',
        '03' => 'марта',
        '04' => 'апреля',
        '05' => 'мая',
        '06' => 'июня',
        '07' => 'июля',
        '08' => 'августа',
        '09' => 'сентября',
        '10' => 'октября',
        '11' => 'ноября',
        '12' => 'декабря'
    ];
    private $days = [
        '1' => "Понедельник",
        '2' => "Вторник",
        '3' => "Среда",
        '4' => "Четверг",
        '5' => "Пятница",
        '6' => "Суббота",
        '0' => "Воскресенье"
    ];
    private $daysShort = [
        '1' => "Пн",
        '2' => "Вт",
        '3' => "Ср",
        '4' => "Чт",
        '5' => "Пт",
        '6' => "Сб",
        '0' => "Вс"
    ];
    private $colors = [
        "red",
        "green",
        "blue",
        "orange",
        "pink",
        "brown",
        "yellow"
    ];
    public $paymentMethodsExt = [
        2 => "qiwi2",
        3 => "wmr2",
        4 => "yandex",
        5 => "yandex_sber",
        6 => "mgfinplatrur",
        7 => "mtsmc",
        8 => "tele2",
        9 => "mobile_beeline"
    ];
    public static function getCridentials($event_id) {
        // var_dump($event_id);
        $mysqli = self::getDBInstance();
        $sql = "select value from b_iblock_property_enum where id in (select value from b_iblock_element_property where IBLOCK_ELEMENT_ID in (SELECT distinct ep.IBLOCK_ELEMENT_ID FROM b_iblock_element_property ep
            WHERE ep.value = " . intval($event_id) . " and ep.IBLOCK_PROPERTY_ID = 70) and IBLOCK_PROPERTY_ID = 71)";
        $stmt = $mysqli->query($sql);
        $row = $stmt->fetch_row();
        $creds = explode(":", $row[0]);
        if (count($creds) == 3)
            $creds = [
                $creds[1],
                $creds[2]
            ];
        elseif (count($creds) == 4) {
            $creds = [
                $creds[1],
                $creds[2],
                $creds[3]
            ];
        }
        return $creds;
    }
    public static function isBlackFriday($event_id) {
        if ($event_id == 88486 || $event_id == 90334)
            return true;
        $mysqli = self::getDBInstance();
        $sql = "SELECT value FROM b_iblock_element_property where IBLOCK_PROPERTY_ID = 60 and IBLOCK_ELEMENT_ID in (
            SELECT distinct ep.IBLOCK_ELEMENT_ID FROM b_iblock_element_property ep
            WHERE ep.value = " . $event_id . " and ep.IBLOCK_PROPERTY_ID = 70)";
        $stmt = $mysqli->query($sql);
        $arr = [];
        while ($row = $stmt->fetch_row()) {
            $arr[] = $row[0];
        }

        if (in_array(BLACK_FRIDAY_ID, $arr) || in_array(BLACK_FRIDAY_ID_2, $arr))
            return true;
        return false;
    }
    public static function isFanticket($event_id) {
            $mysqli = self::getDBInstance();
            $sql = "select value from b_iblock_property_enum where id in (select value from b_iblock_element_property where IBLOCK_ELEMENT_ID in (SELECT distinct ep.IBLOCK_ELEMENT_ID FROM b_iblock_element_property ep
            WHERE ep.value = " . $event_id . " and ep.IBLOCK_PROPERTY_ID = 70) and IBLOCK_PROPERTY_ID = " . Config::$fanticket_prop . ")";
            $stmt = $mysqli->query($sql);
            $row = $stmt->fetch_row();
            if ($row[0] == "Да")
                return true;
            return false;
    }
    public static function getMarker($event_id) {
        $mysqli = self::getDBInstance();
        $sql = "select name from b_iblock_element where id in (
                SELECT value FROM b_iblock_element_property where IBLOCK_PROPERTY_ID = 85 and IBLOCK_ELEMENT_ID in (
                        SELECT distinct ep.IBLOCK_ELEMENT_ID FROM b_iblock_element_property ep
                        WHERE ep.value = " . $event_id . " and ep.IBLOCK_PROPERTY_ID = 70))";
        $stmt = $mysqli->query($sql);
        $row = $stmt->fetch_row();
        if ($row[0])
            return htmlspecialchars($row[0], ENT_QUOTES);
        return false;
    }
    public static function getBitrixNameByEventId($event_id) {
        $mysqli = self::getDBInstance();
        $sql = "select name from b_iblock_element where id in (
            SELECT distinct ep.IBLOCK_ELEMENT_ID FROM b_iblock_element_property ep
            WHERE ep.value = " . intval($event_id) . " and ep.IBLOCK_PROPERTY_ID = 70)";
        $stmt = $mysqli->query($sql);
        $row = $stmt->fetch_row();
        return htmlspecialchars($row[0], ENT_QUOTES);
    }
    public static function getBitrixIdByEventId($event_id) {
        $mysqli = self::getDBInstance();
        $sql = "SELECT IBLOCK_ELEMENT_ID FROM b_iblock_element_property ep
            WHERE ep.value = " . intval($event_id) . " and ep.IBLOCK_PROPERTY_ID = 70";
        $stmt = $mysqli->query($sql);
        $row = $stmt->fetch_row();
        return $row[0];
    }
    public static function getUserDataByEventId($event_id) {
        $mysqli = self::getDBInstance();
        $sql = "SELECT value FROM b_iblock_element_property where IBLOCK_PROPERTY_ID = 76 and IBLOCK_ELEMENT_ID in (
            SELECT distinct ep.IBLOCK_ELEMENT_ID FROM b_iblock_element_property ep
            WHERE ep.value = " . intval($event_id) . " and ep.IBLOCK_PROPERTY_ID = 70
            )";
        $stmt = $mysqli->query($sql);
        $row = $stmt->fetch_row();
        if ($row[0]) {
            $sql = "select personal_mobile from b_user where id=" . $row[0];
            $stmt = $mysqli->query($sql);
            $row = $stmt->fetch_row();
            return [
                "PERSONAL_MOBILE" => $row[0]
            ];
        }
    }
    public static function getExcludedZones($event_id) {
        $mysqli = self::getDBInstance();
        $sql = "SELECT value FROM b_iblock_element_property where IBLOCK_PROPERTY_ID = 83 and IBLOCK_ELEMENT_ID in (
            SELECT distinct ep.IBLOCK_ELEMENT_ID FROM b_iblock_element_property ep
            WHERE ep.value = " . intval($event_id) . " and ep.IBLOCK_PROPERTY_ID = 70)";
        $stmt = $mysqli->query($sql);
        $row = $stmt->fetch_row();
        if ($row[0]) {
            $arr = [];
            $ids = explode(",", $row[0]);
            foreach($ids as $value)
                $arr[] = trim($value);
            return $arr;
        } else
            return [];
    }
    public function __construct($request) {
        Helper::queryLog($request);
        switch($request['view']) {
            case "desktop":
                $this->view = "desktop";
                break;
            case "mobile":
                $this->view = "mobile";
                break;
            default:
                $this->view = "desktop";
                break;
        }
        $this->initProvider($request);
    }
    private function initProvider($request) {
        $creds = [];
        if ($request["event_id"]) {
            $creds = self::getCridentials($request["event_id"]);
        }
        switch($request['provider']) {
            case "ticketsteam":
                $this->provider = new Ticketsteam($this->view, $creds);
                break;
            case "radario":
                $this->provider = new Radario($this->view, $creds);
                break;
            default:
                if (isset($request["orderId"]) || isset($request["payment_info"]) || isset($request["order_id"])) {
                    $mysqli = self::getDBInstance();
                    $sql = "select system, event_id, shop_order_id, status from sgg_orders where bank_order_id = ?";
                    $stmt = $mysqli->prepare($sql);
                    if (isset($request["orderId"]))
                        $stmt->bind_param('s', $request["orderId"]);
                    elseif (isset($request['payment_info'])) {
                        $payment_info = json_decode(base64_decode($request['payment_info']), 1);
                        Helper::log('payment_info:');
                        Helper::log($payment_info);
                        $request["transaction_id"] = $payment_info["transaction_id"];
                        Helper::log('transaction_id: ' . $request["transaction_id"]);
                        $stmt->bind_param('s', $request["transaction_id"]);
                    } elseif (isset($request["order_id"])) {
                        $sql = "select system, event_id, shop_order_id, status from sgg_orders where shop_order_id = ?";
                        $stmt = $mysqli->prepare($sql);
                        $stmt->bind_param('s', $request["order_id"]);
                    }

                    $stmt->bind_result($provider, $event_id, $shop_order_id, $status);
                    $stmt->execute();
                    $stmt->fetch();
                    if ((isset($request['payment_info']) || isset($request["order_id"])) && !$status) {
                        sleep(2);
                        Helper::log('reiniting provider');
                        $this->initProvider($request);
                        return;
                    }

                    $provider = ucfirst($provider);
                    Helper::log('shop_order_id: ' . $shop_order_id);
                    Helper::log('provider: ' . $provider);
                    $stmt->free_result();
                    $creds = self::getCridentials($event_id);
                    $this->provider = new $provider($this->view, $creds);
                }
                break;
        }
    }
    public static function getSmartyInstance() {
        if (!self::$smarty_instance) {
            self::$smarty_instance = new Smarty();
            // $smarty->force_compile = true;
            self::$smarty_instance->debugging = false;
            self::$smarty_instance->caching = false;
            self::$smarty_instance->cache_lifetime = 0;
        }
        return self::$smarty_instance;
    }
    public static function getDBInstance() {
        if (!self::$db_instance) {
            self::$db_instance = new mysqli(Config::$db_host, Config::$db_user, Config::$db_pass, Config::$db_name);
            self::$db_instance->query("set names utf8");
        }
        return self::$db_instance;
    }
    public function resolve($request) {
        switch($request["action"]) {
            case "init":
                header("Content-Type: text/html; charset=utf-8");
                if (isset($request['orderId']) || isset($request['order_id']) || isset($request['transaction_id'])) {
                    if (!$request["starter"]) {

                        if (isset($request['orderId']))
                            $this->completeOrder($request); // запуск от пользователя, альфабанк
                        else
                            $this->completeOrder2($request); // запуск от пользователя, мейл
                    } elseif ($request["starter"] == "cron") {
                        if (isset($request['orderId']))
                            $this->completeOrder($request, true); // запуск от крона, альфабанк
                        else
                            $this->completeOrder2($request, true); // запуск от крона, мейл
                    }
                } elseif (isset($request["payment_info"])) {
                    $payment_info = json_decode(base64_decode($request['payment_info']), 1);
                    $request["transaction_id"] = $payment_info["transaction_id"];
                    $this->completeOrder2($request);
                } else
                    $this->init($request);
                break;
            case "hall_popup":
                header("Content-Type: text/html; charset=utf-8");
                $this->hallPopup($request);
                break;
            case "popup":
                header("Content-Type: text/html; charset=utf-8");
                $this->popup($request);
                break;
            case "add_ticket":
                header("Content-Type: application/json; charset=utf-8");
                $this->addTicket($request);
                break;
            case "cart":
                header("Content-Type: text/html; charset=utf-8");
                $this->cart($request);
                break;
            case "create_order":
                $this->createOrder($request);
                break;
            case "check_promo":
                header("Content-Type: application/json; charset=utf-8");
                $this->checkPromo($request);
                break;
            case "recalculate_cart":
                header("Content-Type: application/json; charset=utf-8");
                $this->recalculateCart($request);
                break;
            default:
                throw new Exception("wrong method");
                break;
        }
    }
    private function checkPromo($request) {
        $request["promocode"] = trim($request["promocode"]);
        $result = $this->provider->checkPromo($request);
        echo json_encode($result);
    }
    private function getPaysystemId($event_id) {
        $sql = "select value from b_iblock_property_enum where id in (select value from b_iblock_element_property where IBLOCK_ELEMENT_ID in (SELECT distinct ep.IBLOCK_ELEMENT_ID FROM b_iblock_element_property ep
            WHERE ep.value = " . intval($event_id) . " and ep.IBLOCK_PROPERTY_ID = 70) and IBLOCK_PROPERTY_ID = 77)";
        $mysqli = self::getDBInstance();
        $stmt = $mysqli->query($sql);
        $row = $stmt->fetch_row();
        if ($row[0] == "Mail.ru")
            return 2;
        elseif ($row[0] == "Альфабанк")
            return 1;
        else
            return Config::$default_paysystem;
    }
    private function getPaymentMethodsExt($event_id) {
        $sql = "select XML_ID, VALUE from b_iblock_property_enum where id in (select value from b_iblock_element_property where IBLOCK_ELEMENT_ID in (SELECT distinct ep.IBLOCK_ELEMENT_ID FROM b_iblock_element_property ep
                WHERE ep.value = " . intval($event_id) . " and ep.IBLOCK_PROPERTY_ID = 70) and IBLOCK_PROPERTY_ID = 84)";
        $mysqli = self::getDBInstance();
        $stmt = $mysqli->query($sql);
        $result = [];
        while ($row = $stmt->fetch_row()) {
            if (isset($this->paymentMethodsExt[$row[0]])) {
                $result[$row[0]] = [
                    'name' => $row[1],
                    'value' => $this->paymentMethodsExt[$row[0]]
                ];
            }
        }
        return $result;
    }
    private function checkSpecialEventsBeforeBuy($request) {
        $mysqli = self::getDBInstance();
        $order_id = null;
        if ($request["event_id"] == SPECIAL_EVENT_ID || $request["event_id"] == SPECIAL_EVENT_ID_2) {
            $response = json_decode(file_get_contents('http://www.radiorecord.ru/api/?f=isRecordFamily&mail=' . $request["email"]));
            if ($response->result == true) {
                $stmt = $mysqli->prepare("select id from sgg_orders where email = ? and status = 2 and event_id in (" . SPECIAL_EVENT_ID . ", " . SPECIAL_EVENT_ID_2 . ")");
                $stmt->bind_param('s', $request["email"]);
                $stmt->bind_result($order_id);
                $stmt->execute();
                $stmt->fetch();
                $stmt->free_result();
                if ($order_id > 0) {
                    $this->error("Вы уже купили билеты по специальной цене. Предпродажа доступна только для одного заказа.", true);
                    die();
                }
            } else {
                $this->error('Предпродажа доступна только для членов Record Family. Вы ввели не зарегистрированный в системе e-mail.<br/><br/><a target="_blank" style="color:#e32b7a" href="https://www.radiorecord.ru/family/">Зарегистрироваться</a>', true);
                die();
            }
        }
    }
    private function createOrder($request) {

        $request["email"] = trim($request["email"]);
        if ($_COOKIE["utm_source"])
            $request["utm_source"] = $_COOKIE["utm_source"];
        $phone = preg_replace("/[^0-9]/", "", $request["phone"]);
        $phone = str_replace("+", "", $phone);
        if (in_array($request["payment_type"], [
            2,
            6,
            7,
            8,
            9
        ])) {
            if (!$phone) {
                $this->error("Не указан телефон", true);
                die();
            }
            if (strlen($phone) != 11) {
                $this->error("Указан некорректный номер", true);
                die();
            }
        }
        $this->checkSpecialEventsBeforeBuy($request);
        $mysqli = self::getDBInstance();
        $result = $this->provider->createOrder($request);
        $order_id = $result['id'];
        $priceSummary = $result['price'];
        $priceSummary = $priceSummary * 100;

        $order_paysystem = $order_id;
        $paysystem = $this->getPaysystemId($request["event_id"]);
        Helper::log('paysystem: ' . $paysystem);
        if ($this->provider->name == 'radario')
            $order_paysystem = "UserOrder-" . $order_paysystem;
        else if ($this->provider->name == 'ticketsteam') {
            $order_paysystem = $this->provider->apiQueryExt([
                'order_id' => $order_paysystem,
                'action' => 'order.create',
                'price' => $result['price'],
                'api_key' => 'skljdfjkf7890awdDJAOHUasioNDJ',
                'payment_system' => $paysystem
            ]);
            $order_paysystem = $order_paysystem->id;
        }

        if ($paysystem == 1) {
            $response = RBS::register([
                "orderNumber" => $order_paysystem, // в альфабанк передается id транзакции в ticketsteam или UserOrder-id заказа в радарио
                "description" => $request["description"],
                "amount" => $priceSummary,
                "mode" => $request["mode"],
                "pageView" => strtoupper($this->view),
                "theme" => $request["theme"]
            ]);
        } elseif ($paysystem == 2) {
            $mail_order_id = $order_id;
            if ($this->provider->name == 'radario')
                $mail_order_id = "UserOrder-" . $order_id;
            $data = [
                "order_amount" => $result['price'],
                "order_id" => $mail_order_id, // в mail передается номер заказа в ticketsteam или UserOrder-id заказав радарио
                "user_ip" => $_SERVER[Config::$client_ip_param],
                "user_login" => $request["email"]
            ];
            if ($request["theme"])
                $data["theme"] = $request["theme"];
            if ($request["mode"])
                $data["mode"] = $request["mode"];

            $this->processAdNetworksBeforeComplete($request, $data);
            // получение доп. способов оплаты - только для mail.ru эквайринга
            $paymentMethodsExt = $this->getPaymentMethodsExt($request['event_id']);

            // Helper::log($data);
            if ($paysystem == 2 && (!$request["payment_type"] || $request["payment_type"] == 1)) {
                $vkp = new VKP();
                $url = $vkp->paymentUrl($data);
                if (strpos($url, 'Success=False') !== false) {
                    parse_str($url, $arr);
                    $response = [
                        'errorCode' => -1,
                        'errorMessage' => $arr["descr"]
                    ];
                } else
                    $response = [
                        'formUrl' => $url
                    ];
            } elseif ($paysystem == 2 && in_array($request["payment_type"], array_keys($paymentMethodsExt))) {
                $mail = new Mail();
                $paymentMethod = '';
                if ($request["payment_type"] == 2) {
                    $paymentMethod = 'QIWI2';
                    Helper::log('phone:' . $phone);
                } elseif ($request["payment_type"] == 3) {
                    $paymentMethod = 'WMR2';
                } elseif ($request["payment_type"] == 4) {
                    $paymentMethod = 'YANDEX';
                } elseif ($request["payment_type"] == 5) {
                    $paymentMethod = 'YANDEX_SBER';
                } elseif ($request["payment_type"] == 6) {
                    /*
                     * Привет.
                     * Не помню тестировали ли вы мобильную коммерцию, поэтому напоминаю ее pay_method :
                     * МТС: mtsmc
                     * Билайн: mobile_beeline
                     * Мегафон: mgfinplatrur
                     * Теле2: tele2
                     */
                    $paymentMethod = 'mgfinplatrur';
                } elseif ($request["payment_type"] == 7) {
                    $paymentMethod = 'mtsmc';
                } elseif ($request["payment_type"] == 8) {
                    $paymentMethod = 'tele2';
                } elseif ($request["payment_type"] == 9) {
                    $paymentMethod = 'mobile_beeline';
                }
                if (in_array($request["payment_type"], [
                    6,
                    7,
                    8,
                    9
                ]))
                    $request["description"] = "Showgogo";
                $tran = $mail->transaction($paymentMethod, $result['price'], $data['order_id'], $request["description"], $data["user_login"], $phone, $data);
                Helper::log('transaction start:');
                Helper::log($tran);
                if ($request["payment_type"] == 2) {
                    $response = [
                        'text' => '<div class="sgg-widget-postpay-message">Вам выставлен счет в <a target="_blank" href="https://qiwi.com/payment/order.action">личном кабинете QIWI</a>. Оплатите его в течении 15 минут. <br/>Ваши билеты будут отправлены на ' . htmlentities($request["email"]) . ' после оплаты счета</div>'
                    ];
                } elseif (in_array($request["payment_type"], [
                    3,
                    4,
                    5
                ])) {
                    if ($request["payment_type"] == 3)
                        $ps = 'Webmoney';
                    elseif ($request["payment_type"] == 4)
                        $ps = 'Yandex деньги';
                    elseif ($request["payment_type"] == 5)
                        $ps = 'Сбербанк Онлайн';
                    $response = [
                        'text' => '<div class="sgg-widget-postpay-message">Вам выставлен счет в <a target="_blank" href="' . $tran["action_param"]["url"] . '">личном кабинете ' . $ps . '</a>. Оплатите его в течении 15 минут. <br/>Ваши билеты будут отправлены на ' . htmlentities($request["email"]) . ' после оплаты счета</div>'
                    ];
                } elseif (in_array($request["payment_type"], [
                    6,
                    7,
                    8,
                    9
                ])) {
                    $response = [
                        'text' => '<div class="sgg-widget-postpay-message">Вам выставлен счет на мобильный телефон ' . $phone . '. Оплатите его в течении 15 минут. <br/>Ваши билеты будут отправлены на ' . htmlentities($request["email"]) . ' после оплаты счета</div>'
                    ];
                } else if ($tran["action_param"]["url"]) {
                    $response = [
                        'formUrl' => $tran["action_param"]["url"]
                    ];
                }
            }
        } // elseif ($paysystem == 2) end
        if (isset($response['errorCode'])) { // В случае ошибки вывести ее
            $this->error($response['errorMessage'], true);
        } else { // В случае успеха перенаправить пользователя на плетжную форму\
            include_once $_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php";
            global $USER;
            $bx_user_id = null;
            if ($USER->GetID())
                $bx_user_id = $USER->GetID();
            $sql = "insert into sgg_orders(name, phone, email, shop_order_id, bank_order_id, system, price, event_id, user_agent, paysystem_id, payment_method_id, bx_user_id) values (?, ?, ?, ?, ?, '" . $this->provider->name . "', ?, ?, ?, ?, ?, ?)";
            $stmt = $mysqli->prepare($sql);
            $stmt->bind_param('sssissisiii', $request["name"], $request["phone"], $request["email"], $order_id, $response['orderId'], $priceSummary, intval($request["event_id"]), $_SERVER["HTTP_USER_AGENT"], $paysystem, intval($request["payment_type"]), $bx_user_id);
            $stmt->execute();
            $stmt->close();
            $mysqli->close();
            // header('Location: ' . $response['formUrl']);
            $response['order_id'] = $order_paysystem;
            header("Content-Type: application/json; charset=utf-8");
            echo json_encode($response);
        }
    }
    private function error($string, $ajax = false) {
        if (!$ajax) {
            $smarty = self::getSmartyInstance();
            $smarty->assign([
                "string" => $string
            ]);
            $smarty->display(__DIR__ . '/../tpl/' . $this->view . '/error.html');
        } else {
            header("Content-Type: application/json; charset=utf-8");
            echo json_encode([
                "message" => $string
            ]);
        }
    }
    private function sendEmailAfterBuyIfNeeded($result) {
        $user = self::getUserDataByEventId($result["id"]);
        if ($user["PERSONAL_MOBILE"]) {
            $message = "Были приобретены билеты:";
            foreach($result['data'] as $seat) {
                $message .= " " . $seat["name"] . ", " . $seat["datestring"] . ", " . $seat["sector_name"];
                if ($seat["row_and_place"])
                    $message .= ", " . $seat["row_and_place"];
                $message .= "; ";
            }
            include_once $_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php";
            CModule::IncludeModule('rarus.sms4b');
            global $SMS4B;
            $SMS4B->SendSMS($message, $user["PERSONAL_MOBILE"]);
            // $answer = file_get_contents('https://gate.smsaero.ru/send/?user=asaulin@radiorecord.ru&password=wOHSyBZRKx2ilyjr4vuqjKG6DzZx&to=79214229889&text=' .
            // urlencode($message) . '&from=biznes&type=5');
        }
    }
    private function processAdNetworksBeforeComplete($request, &$data) {
        session_start();
        if ($request["event_id"] == 3569 || $request["event_id"] == 943) {
            if ($_SESSION['admitad_uid'])
                $data['admitad_uid'] = $_SESSION['admitad_uid'];
            if ($_SESSION['actionpay'])
                $data['actionpay'] = $_SESSION['actionpay'];
            if ($_SESSION['_1lr'])
                $data['_1lr'] = $_SESSION['_1lr'];
            if ($_SESSION['_1larg_sub'])
                $data['_1larg_sub'] = $_SESSION['_1larg_sub'];
        }
        session_write_close();
    }
    private function processAdNetworksAfterComplete($result, $shop_order_id) {
        session_start();
        if ($_SESSION['admitad_uid'] && $result['price']) {
            if ($result["id"] == 3569 || $result["id"] == 943) {
                Helper::log('sending admitad_uid: ' . $_SESSION['admitad_uid']);
                file_get_contents('https://ad.admitad.com/r?campaign_code=d9ebc4b22d&pb=1&pk=43B73612a5F80C9F7A1f0673954573AC&ac=4&uid=' . $_SESSION['admitad_uid'] . '&oid=' . $shop_order_id . '&tc=1&price=' . $result['price']);
            }
        }
        if ($_SESSION['actionpay'] && $result['price']) {
            if ($result["id"] == 3569 || $result["id"] == 943) {
                Helper::log('sending actionpay: ' . $_SESSION['actionpay']);
                file_get_contents('https://x.actionpay.ru/ok/15061.png?actionpay=' . $_SESSION['actionpay'] . '&apid=' . $shop_order_id . '&price=' . $result['price']);
            }
        }
        session_write_close();
    }
    private function downloadTicketIfNeeded($bank_order_id, $shop_order_id, $result) {
        if (!is_dir(__DIR__ . "/../tickets/" . $bank_order_id)) {
            mkdir(__DIR__ . "/../tickets/" . $bank_order_id);
            $arr = [
                "id" => $shop_order_id
            ];
            $pdf = $this->provider->getTicket($arr, $result['data']);
            if (is_array($pdf)) { // Radario
                foreach($pdf as $key => $value) {
                    file_put_contents(__DIR__ . "/../tickets/" . $bank_order_id . "/order-" . $key . '.pdf', $value);
                }
            } else { // Ticketsteam
                $pdf = base64_decode($pdf);
                // var_dump($pdf);
                file_put_contents(__DIR__ . "/../tickets/" . $bank_order_id . "/order-" . $shop_order_id . '.pdf', $pdf);
            }
        }
    }
    /**
     *
     * Завершение заказа mail.ru
     *
     * @param array $request
     * @param bool $cron
     */
    private function completeOrder2($request, $cron = false) {
        $mysqli = self::getDBInstance();
        if (isset($request['order_id'])) {
            if (!$cron && $request["sgg_token"] != Helper::getToken($request['order_id'])) {
                $this->error("Неверный токен заказа");
                die();
            }
        }
        if (isset($request['transaction_id'])) // оплата по карте
            $sql = "select id, shop_order_id, bank_order_id, email, ticket_sent, status, event_id, name, phone, bx_user_id from sgg_orders where bank_order_id = ?";
        else // оплата другим способом
            $sql = "select id, shop_order_id, bank_order_id, email, ticket_sent, status, event_id, name, phone, bx_user_id from sgg_orders where shop_order_id = ?";
        $stmt = $mysqli->prepare($sql);
        if (isset($request['transaction_id']))
            $stmt->bind_param('s', $request["transaction_id"]);
        else
            $stmt->bind_param('s', $request["order_id"]);
        $shop_order_id = null;
        $order_id = null;
        $bank_order_id = null;
        $email = null;
        $status = null;
        $ticket_sent = null;
        $event_id = null;
        $name = null;
        $phone = null;
        $bx_user_id = null;
        $stmt->bind_result($order_id, $shop_order_id, $bank_order_id, $email, $ticket_sent, $status, $event_id, $name, $phone, $bx_user_id);
        $stmt->execute();
        $stmt->fetch();
        /*
         * if ($status === null) {
         * sleep(1);
         * $this->completeOrder2($request);
         * }
         */
        $stmt->free_result();
        if ($cron && $status != 2) {
            Helper::log('cron stopping on order id:' . $shop_order_id . ', bank_order_id: ' . $bank_order_id . ', our status: ' . $status);
            return;
        }
        if (!$bank_order_id)
            $bank_order_id = $shop_order_id;
        $sql = "update sgg_orders set time_updated = now(), ticket_sent = ?, ip = ?, bx_user_id = ? where id = ?";
        $stmt = $mysqli->prepare($sql);
        Helper::log('updating order id: ' . $shop_order_id . ', bank_order_id: ' . $bank_order_id . ', our status: ' . $status);

        $smarty = self::getSmartyInstance();
        $first_sent = 0;
        if ($status == 2) {
            $result = $this->provider->getOrder($shop_order_id, $bank_order_id);
            if (!$ticket_sent) {
                if (!$cron)
                    Helper::log('order payed, sending tickets by user on order id: ' . $shop_order_id . ' because of our status: ' . $status . ' and aquire is mail.ru');
                else
                    Helper::log('order payed, sending tickets by cron on order id: ' . $shop_order_id . ' because of our status: ' . $status . ' and aquire is mail.ru');
                $this->provider->completeOrder($shop_order_id);
                $ticket_sent = 1;
                $first_sent = 1;
                $this->sendEmailAfterBuyIfNeeded($result);
                $this->processAdNetworksAfterComplete($result, $shop_order_id);

				// Регистрация
                include_once $_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php";
	            $rsUsers = CUser::GetList($by = "id", $order = "asc", array('=EMAIL' => $email));
	            if($rsUsers->result->num_rows == 0) {
	                if (!$bx_user_id) { //неавторизованный новый пользователь
    		            $new_name = array('', '');
    		            if($name) {
    			            $new_name = explode(" ", $name);
    		            }
    		            $new_password = randString(7);
    		            $new_user = new CUser;
    		            $arFields = Array(
    			            "NAME"              => $new_name[0],
    			            "LAST_NAME"         => $new_name[1],
    			            "EMAIL"             => $email,
    			            "LOGIN"             => $email,
    			            "PERSONAL_PHONE"    => $phone,
    			            "LID"               => "ru",
    			            "ACTIVE"            => "Y",
    			            "GROUP_ID"          => array(3, 4, 5),
    			            "UF_SUBSCRIBE"      => true,
    			            "PASSWORD"          => $new_password,
    			            "CONFIRM_PASSWORD"  => $new_password,
    			            "USER_PASSWORD"  => $new_password
    		            );
    		            $bx_user_id = $new_user->Add($arFields);
	                } else { //пользователь из соц. сети с незаполненным емайлом
	                    global $USER;
	                    $arFields = Array("EMAIL" => $email);
	                    $USER->Update($bx_user_id, $arFields);
	                }
	            }

                // Начисление бонусов
                if (CModule::IncludeModule("awg.bonus")) {
                    $tmpUser = CUser::GetByLogin($email);
                    $curUser = $tmpUser->Fetch();
                    $addBonus = awgBonus::changeBonus($curUser["ID"], 4, $result["price"], self::getBitrixIdByEventId($event_id), count($result['data']), $result["data"]);
                }
            } else
                Helper::log('not sending tickets on order id: ' . $shop_order_id . ' because of our status: ' . $status . ' and and aquire is mail.ru and ticket was sent');

            if ($result['ticket_quantity'] > 0) {
                $smarty->assign("contacts", $result['data']);
            }
            $smarty->assign([
                "ticket_quantity" => $result['ticket_quantity'],
                "email" => $email,
                "order_id" => $shop_order_id,
                "real_name" => self::getBitrixNameByEventId($result["id"]),
                "name" => $result['data'][0]["name"] ? $result['data'][0]["name"] : "",
                "first" => ($first_sent && $status == 2) ? '1' : '0',
                "event_id" => $event_id,
                "bx_event_id" => self::getBitrixIdByEventId($event_id)
            ]);

            $this->downloadTicketIfNeeded($bank_order_id, $shop_order_id, $result);
            $ts_status = 1;
        } else {
            Helper::log('cancelling order id: ' . $shop_order_id . ', our status: ' . $status . ' and aquire is mail.ru');
            $this->provider->cancelOrder($shop_order_id);
            $smarty->assign([
                "error" => "Невозможно совершить платеж",
                "ticket_quantity" => 0
            ]);
            $ts_status = 2;
        }
        $ip_addr = $_SERVER[Config::$client_ip_param];
        $stmt->bind_param('isii', $ticket_sent, $ip_addr, $bx_user_id, $order_id);
        $stmt->execute();
        $this->provider->afterComplete($shop_order_id, $bank_order_id, $ts_status);
        if (!$cron)
            $smarty->display(__DIR__ . '/../tpl/' . $this->view . '/complete.html');
    }
    /**
     *
     * Завершение заказа alphabank
     *
     * @param array $request
     * @param bool $cron
     */
    private function completeOrder($request, $cron = false) {
        $mysqli = self::getDBInstance();
        $sql = "select id, shop_order_id, bank_order_id, email, status, event_id, name, phone, bx_user_id from sgg_orders where bank_order_id = ?";
        $stmt = $mysqli->prepare($sql);
        $stmt->bind_param('s', $request["orderId"]);
        $shop_order_id = null;
        $order_id = null;
        $bank_order_id = null;
        $email = null;
        $status = null;
        $event_id = null;
	    $name = null;
	    $phone = null;
	    $bx_user_id = null;
	    $stmt->bind_result($order_id, $shop_order_id, $bank_order_id, $email, $status, $event_id, $name, $phone, $bx_user_id);
        $stmt->execute();
        $stmt->fetch();
        $response = RBS::getOrderStatus([
            "orderId" => $bank_order_id
        ]);
        $stmt->free_result();
        if ($cron && $response['OrderStatus'] != 2) {
            Helper::log('cron stopping on order id:' . $shop_order_id . ', bank_order_id: ' . $bank_order_id . ', our status: ' . $status . ', bank status: ' . $response['OrderStatus']);
            return;
        }
        $sql = "update sgg_orders set status = ?, error_code = ?, error_message = ?, cardholder = ?, card = ?, amount = ?, ip = ?, time_updated = now() where id = ?";
        $stmt = $mysqli->prepare($sql);
        Helper::log('updating order id: ' . $shop_order_id . ', bank_order_id: ' . $bank_order_id . ', our status: ' . $status . ', bank status: ' . $response['OrderStatus']);
        $stmt->bind_param('iisssisi', $response['OrderStatus'], $response['ErrorCode'], $response['ErrorMessage'], $response['cardholderName'], $response['Pan'], $response['Amount'], $response['Ip'], $order_id);
        $sql2 = "update sgg_orders set ticket_sent = ? where id = ?";
        $stmt2 = $mysqli->prepare($sql2);
        $ticket_sent = 0;
        $smarty = self::getSmartyInstance();
        if ($response['OrderStatus'] == 2) {
            if ($status != 2 && $response['OrderStatus'] == 2) {
                if ($cron) {
                    Helper::log('order payed, sending tickets by cron on order id: ' . $shop_order_id . ' because of our status: ' . $status . ' and bank status: ' . $response['OrderStatus']);
                } else {
                    Helper::log('order payed, sending tickets by user on order id: ' . $shop_order_id . ' because of our status: ' . $status . ' and bank status: ' . $response['OrderStatus']);
                }
                $this->provider->completeOrder($shop_order_id);
                $ticket_sent = 1;
            } else
                Helper::log('not sending tickets on order id: ' . $shop_order_id . ' because of our status: ' . $status . ' and bank status: ' . $response['OrderStatus']);
            $result = $this->provider->getOrder($shop_order_id, $bank_order_id);

            if ($result['ticket_quantity'] > 0) {
                $smarty->assign("contacts", $result['data']);
                if ($status != 2 && $response['OrderStatus'] == 2) {
                    $this->sendEmailAfterBuyIfNeeded($result);
                    $this->processAdNetworksAfterComplete($result, $shop_order_id);

	                // Регистрация
                    include_once $_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php";
	                $rsUsers = CUser::GetList($by = "id", $order = "asc", array('=EMAIL' => $email));
	                if($rsUsers->result->num_rows == 0) {
	                    if (!$bx_user_id) { //неавторизованный новый пользователь
	                        $new_name = array('', '');
	                        if($name) {
	                            $new_name = explode(" ", $name);
	                        }
	                        $new_password = randString(7);
	                        $new_user = new CUser;
	                        $arFields = Array(
	                                "NAME"              => $new_name[0],
	                                "LAST_NAME"         => $new_name[1],
	                                "EMAIL"             => $email,
	                                "LOGIN"             => $email,
	                                "PERSONAL_PHONE"    => $phone,
	                                "LID"               => "ru",
	                                "ACTIVE"            => "Y",
	                                "GROUP_ID"          => array(3, 4, 5),
	                                "UF_SUBSCRIBE"      => true,
	                                "PASSWORD"          => $new_password,
	                                "CONFIRM_PASSWORD"  => $new_password,
	                                "USER_PASSWORD"  => $new_password
	                                );
	                        $bx_user_id = $new_user->Add($arFields);
	                        $mysqli->query("update sgg_orders set bx_user_id = $bx_user_id where id = $order_id");
	                    } else { //пользователь из соц. сети с незаполненным емайлом
	                        global $USER;
	                        $arFields = Array("EMAIL" => $email);
	                        $USER->Update($bx_user_id, $arFields);

	                    }
	                }

                    // Начисление бонусов
                    if (CModule::IncludeModule("awg.bonus")) {
                        $tmpUser = CUser::GetByLogin($email);
                        $curUser = $tmpUser->Fetch();
                        $addBonus = awgBonus::changeBonus($curUser["ID"], 4, $result["price"], self::getBitrixIdByEventId($event_id), count($result['data']), $result["data"]);
                    }
                }
            }
            $smarty->assign([
                "ticket_quantity" => $result['ticket_quantity'],
                "email" => $email,
                "order_id" => $shop_order_id,
                "real_name" => self::getBitrixNameByEventId($result["id"]),
                "name" => $result['data'][0]["name"] ? $result['data'][0]["name"] : "",
                "first" => ($status != 2) ? '1' : '0',
                "event_id" => $event_id,
                "bx_event_id" => self::getBitrixIdByEventId($event_id)
            ]);
            $this->downloadTicketIfNeeded($bank_order_id, $shop_order_id, $result);
            $ts_status = 1;
        } else {
            Helper::log('cancelling order id: ' . $shop_order_id . ', our status: ' . $status . ' and bank status: ' . $response['OrderStatus']);
            $this->provider->cancelOrder($shop_order_id);
            $smarty->assign([
                "error" => $response['ErrorMessage'],
                "ticket_quantity" => 0
            ]);
            $ts_status = 2;
        }
        $stmt->execute();
        $stmt2->bind_param('ii', $ticket_sent, $order_id);
        $stmt2->execute();
        $this->provider->afterComplete($shop_order_id, $bank_order_id, $ts_status);
        if (!$cron)
            $smarty->display(__DIR__ . '/../tpl/' . $this->view . '/complete.html');
    }
    private function addTicket($request) {
        $response = $this->provider->addTicket($request);
        echo $response;
    }
    private function hallPopup($request) {
        $smarty = self::getSmartyInstance();
        $style = '';
        if ($request["mode"] != "widget")
            $style = 'width:' . $request['width'] . 'px;height:' . $request['height'] . 'px;';
        $smarty->assign([
            'style' => $style,
            'mode' => $request["mode"] == "widget" ? 0 : 1
        ]);
        $smarty->display(__DIR__ . '/../tpl/' . $this->view . '/popup_hall.html');
    }

    private function recalculateCart($request) {
        $data = $this->provider->cart($request);
        echo json_encode([
            'status' => 0,
            'price_summary' => $data['price_summary'],
            'price_service' => $data['price_service'] ? $data['price_service']:null
        ]);

    }


    private function cart($request) {
        $data = $this->provider->cart($request);
        $GLOBALS["AWG_TICKET"] = $data["data"];
        //var_dump($data);
        $smarty = self::getSmartyInstance();
        $smarty->assign(["enable_remove" => $this->provider->name == 'ticketsteam' ? 1 : 0]);
        $smarty->assign("contacts", $data["data"]);
        if (!isset($data["price_service"]))
            $data["price_service"] = 0;
        $paysystem_id = $this->getPaysystemId($request["event_id"]);
        $price_summary = $data["price_summary"] + $data["price_service"];
        $mail = '';
        if (($request["event_id"] == SPECIAL_EVENT_ID || $request["event_id"] == SPECIAL_EVENT_ID_2) && $request["promo"]) {
            $rf_mail = json_decode(file_get_contents('http://www.radiorecord.ru/api/?f=emailByEncodedId&id=' . $request["promo"]));
            if ($rf_mail->result && $rf_mail->result !== 'false')
                $mail = $rf_mail->result;
        }
        $payment_methods = $this->getPaymentMethodsExt($request["event_id"]);

        require_once($_SERVER['DOCUMENT_ROOT'].'/widget/bonus.php');

        $user_id = $USER->getID();
        $mail_readonly = 0;
        $username = '';
        $personal_phone = '';
        if ($user_id) {
            $rsUser = CUser::GetByID($user_id);
            $arUser = $rsUser->Fetch();
            //var_dump($arUser);
            if ($arUser["ACTIVE"] == "Y") {
                $mail = $arUser["EMAIL"];
                //$mail_readonly = 1;
                $username = trim($arUser["NAME"] . ' ' . $arUser["LAST_NAME"]);
                $personal_phone = $arUser["PERSONAL_PHONE"];
                if (substr($personal_phone, 0, 1) == '8')
                    $personal_phone = '7' . substr($personal_phone, 1);
            }
        }

        $fanticket = self::isFanticket($request['event_id']);
        $pickup_str = '';
        $fanticket_pic = '';
        if ($fanticket) {
            $pickup_date = WorkCalendar::addWorkDays();
            $pickup_str = WorkCalendar::$days_genitive_case[$pickup_date->format('w')] . ', ' .
                $pickup_date->format('d') . ' ' . $this->monthes[$pickup_date->format('m')];
            $fanticket_pic = $this->getFanticketPictureByEventId($request["event_id"]);
        }

        $data = [
            "may_bonus" => $GLOBALS["BONUS"]["may_bonus"],
            "bx_event_id" => self::getBitrixIdByEventId($request["event_id"]),
            "description" => self::getBitrixNameByEventId($request["event_id"]),
            "price_summary_data" => $data["price_summary"],
            "price_summary" => $price_summary,
            "price_service" => $data["price_service"],
            "ticket_quantity" => $data["ticket_quantity"],
            "ticket_quantity_declinated" => $data["ticket_quantity"] . ' ' . Helper::declination($data["ticket_quantity"], [
                'билет',
                'билета',
                'билетов'
            ]),
            "uid" => $request['uid'],
            "widget_url" => self::$widget_url,
            "provider" => $this->provider->name,
            "ticket_ids" => $data["ticket_ids"],
            "entry_tickets" => $data["entry_tickets"],
            "event_id" => $request["event_id"],
            "mode" => $request["mode"],
            "enable_promo" => 1, // $this->provider->name == 'ticketsteam'?1:0,
            "theme" => $request["theme"],
            "payment_method_selector" => (count($payment_methods) > 0 && $paysystem_id == 2 && $price_summary <= 15000) ? "1" : "0",
            "payment_methods" => $payment_methods,
            "mail" => $mail,
            "mail_readonly" => $mail_readonly,
            "username" => $username,
            "personal_phone" => $personal_phone,
            'fanticket' => $fanticket,
            'pickup_str' => $pickup_str,
            'delivery_price' => Config::$delivery_price,
            'fanticket_price' => Config::$fanticket_price,
            'fanticket_pic' => $fanticket_pic
        ];
        $smarty->assign($data);
        $smarty->display(__DIR__ . '/../tpl/' . $this->view . '/order.html');
    }
    private function popup($request) {
        $result = $this->provider->popup($request);
        $smarty = self::getSmartyInstance();
        $smarty->assign("contacts", $result['prices_arr']);
        $smarty->assign([
            'html' => $result['html'],
            'background' => $result['background'] ? $result['background'] : '',
            'sector_name' => $result['sector_name'],
            'sector_id' => $request['sector_id'],
            'style' => $result['style'],
            'provider' => $this->provider->name,
            'popup_style' => isset($result['popup_style']) ? $result['popup_style'] : "",
            'body_style' => isset($result['body_style']) ? $result['body_style'] : "",
            'zoom' => $this->provider->name == 'ticketsteam' ? 1 : 0,
            'stage' => $result['stage'] ? $result['stage'] : '',
            'mode' => ($request["mode"] != "widget") ? 1 : 0,
            'classname' => count($result['prices_arr']) > 7 ? 'small' : ''
        ]);
        $smarty->display(__DIR__ . '/../tpl/' . $this->view . '/popup.html');
    }
    private function getImageByEventId($event_id) {
        include_once $_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php";
        $mysqli = self::getDBInstance();
        $stmt = $mysqli->query("SELECT distinct ep.IBLOCK_ELEMENT_ID as id FROM b_iblock_element_property ep
            WHERE ep.value = " . intval($event_id) . " and ep.IBLOCK_PROPERTY_ID = 70");
        $row = $stmt->fetch_row();
        $stmt = $mysqli->query("select value from b_iblock_element_property where IBLOCK_ELEMENT_ID = " . $row[0] . " and IBLOCK_PROPERTY_ID = 49");
        $row = $stmt->fetch_row();
        $img = CFile::GetPath($row[0]);
        return $img;
    }
    public function getFanticketPictureByEventId($event_id) {
        include_once $_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php";
        $mysqli = self::getDBInstance();
        $stmt = $mysqli->query("SELECT value FROM b_iblock_element_property where IBLOCK_PROPERTY_ID = " . Config::$fanticket_pic_prop . " and IBLOCK_ELEMENT_ID in (
            SELECT distinct ep.IBLOCK_ELEMENT_ID FROM b_iblock_element_property ep
            WHERE ep.value = " . intval($event_id) . " and ep.IBLOCK_PROPERTY_ID = 70)");
        $row = $stmt->fetch_row();
        $img = CFile::GetPath($row[0]);
        return $img;
    }
    private function getTime($event_id) {
        $mysqli = self::getDBInstance();
        $stmt = $mysqli->query("select value from b_iblock_element_property where IBLOCK_PROPERTY_ID = 51 and IBLOCK_ELEMENT_ID = (
            SELECT distinct IBLOCK_ELEMENT_ID FROM b_iblock_element_property ep
            WHERE ep.value = " . intval($event_id) . " and IBLOCK_PROPERTY_ID = 70) order by value");
        $arr = [];
        while ($row = $stmt->fetch_row()) {
            $arr[] = $row[0];
        }
        // $delimiter = ''; $dateTimeStr = '';

        if (count($arr) > 1) {
            $stmt = $mysqli->query("select value from b_iblock_element_property where IBLOCK_PROPERTY_ID = 70 and IBLOCK_ELEMENT_ID = (
            SELECT distinct IBLOCK_ELEMENT_ID FROM b_iblock_element_property ep
            WHERE ep.value = " . intval($event_id) . " and IBLOCK_PROPERTY_ID = 70)");
            $arr_ids = [];
            while ($row = $stmt->fetch_row()) {
                $arr_ids[] = $row[0];
            }
            $stmt = $mysqli->query("select value from b_iblock_element_property where IBLOCK_PROPERTY_ID = 80 and IBLOCK_ELEMENT_ID = (
            SELECT distinct IBLOCK_ELEMENT_ID FROM b_iblock_element_property ep
            WHERE ep.value = " . intval($event_id) . " and IBLOCK_PROPERTY_ID = 70)");
            $arr_denied = [];
            while ($row = $stmt->fetch_row()) {
                $arr_denied[] = $row[0];
            }
        }
        $dates = [];
        foreach($arr as $key => $value) {
            $date = DateTime::createFromFormat("Y-m-d H:i:s", $value);
            $dateTimeStr = '<span>' . $date->format("d") . ' ' . $this->monthes[$date->format("m")] . '</span> ' . $date->format("H:i") . ' ' . $this->daysShort[$date->format('w')];

            $id = (count($arr) > 1) ? $arr_ids[$key] : $event_id;
            if ($arr_denied[$key] != "Y")
                $dates[] = [
                    'date' => $dateTimeStr,
                    'date_real' => $date->format("d.m.y-H.i"),
                    'event_id' => $id
                ];
        }
        return $dates;
    }
    private function prepareDates($event_dates, $request, &$multiple_dates, &$current_date, &$dates) {
        $multiple_dates = false;
        if (count($event_dates) > 1) {
            $multiple_dates = true;
            if ($this->view == 'mobile') {
                $dates = '<select class="sgg-widget-date-selector">';
                foreach($event_dates as $value) {
                    $selected = $value["event_id"] == $request["event_id"] ? ' selected="selected"' : '';
                    $dates .= '<option data-event_id="' . $value["event_id"] . '" value="' . $value["date_real"] . '" ' . $selected . '>' . $value['date'] . '</option>';
                }
                $dates .= '</select>';
            } else {
                $dates = '';
                $i = 0;
                $hidden = '';
                foreach($event_dates as $value) {
                    if ($value["event_id"] == $request["event_id"]) {
                        $selected = ' selected';
                        $current_date = $value['date'];
                    } else
                        $selected = '';
                    if ($i == 4) {
                        $dates .= '<a class="sgg-widget-dates-show-all can-be-hidden" href="#">Показать все</a>';
                        $hidden = ' hidden can-be-hidden';
                    }
                    $dates .= '<div class="sgg-widget-dates' . $selected . $hidden . '" data-value="' . $value["date_real"] . '" data-event_id="' . $value["event_id"] . '">' . $value['date'] . '</div>';
                    $i++;
                }
            }
        } else {
            $dates = $event_dates[0]['date'];
            $current_date = $event_dates[0]['date'];
        }
    }
    private function processAdNetworksBeforeInit($request) {
        session_start();
        if ($request["event_id"] == 3569 || $request["event_id"] == 943) {
            if ($request['admitad_uid'])
                $_SESSION['admitad_uid'] = $request['admitad_uid'];
            if ($request['actionpay'])
                $_SESSION['actionpay'] = $request['actionpay'];
            if ($request['_1lr'])
                $_SESSION['_1lr'] = $request['_1lr'];
            if ($request['_1larg_sub'])
                $_SESSION['_1larg_sub'] = $request['_1larg_sub'];
        }
        if (isset($request['partner_id'])) {
            if ($request['partner_id'])
                $_SESSION['partner_id'] = $request['partner_id'];
        } else
            unset($_SESSION['partner_id']);
        session_write_close();
    }

    private function getProviderByEventId($event_id) {
        $mysqli = self::getDBInstance();
        $sql = "select VALUE from b_iblock_property_enum where id in (select value from b_iblock_element_property where IBLOCK_ELEMENT_ID in (SELECT distinct ep.IBLOCK_ELEMENT_ID FROM b_iblock_element_property ep
                WHERE ep.value = " . intval($event_id) . " and ep.IBLOCK_PROPERTY_ID = 70) and IBLOCK_PROPERTY_ID = 69)";
        $stmt = $mysqli->query($sql);
        $row = $stmt->fetch_row();
        return $row[0];
    }

    public function getEventData($event_id) {
        $provider = $this->getProviderByEventId($event_id);
        $this->initProvider(["provider" => strtolower($provider), "event_id" => $event_id]);
        $result = $this->provider->init(["event_id" => $event_id]);
        return $result;
    }

    private function init($request) {
        $result = $this->provider->init($request);

        $this->processAdNetworksBeforeInit($request);

        $smarty = self::getSmartyInstance();
        if (substr($result["age"], -1) != '+')
            $result["age"] .= '+';
        if ($result['min_full_price'] == '10000000')
            $prices = '';
        else {
            $prices = $result['min_full_price'];
            if ($result['max_full_price'] != $result['min_full_price'])
                $prices .= ' - ' . $result['max_full_price'];
        }
        $image = $this->getImageByEventId($request['event_id']);
        if ($image) {
            $result['image'] = $image;
            $result['image_preview'] = $image;
        }

        if ($this->provider->name == 'radario' && ($result["version"] == 2 || $result["version"] == 1)) {
            $hall = $result["data"];
        } else {
            // /
            $hall = '';
            $sold = [];
            foreach($result['data'] as $key => $val) {
                if ($result['data'][$key]["tickets"] > 130) {
                    $result['data'][$key]["tickets"] = $result['data'][$key]["tickets"] % 80 + 50;
                }
                if ($result['data'][$key]["tickets"] == 0) {
                    $sold[$key] = $val;
                }
            }
            $empty = array_keys($sold);
            if (!empty($empty)) {
                foreach($empty as $key => $val)
                    unset($result['data'][$val]);
            }
            $result['data'] = array_merge($result['data'], $sold);
        }

        // several dates
        $event_dates = $this->getTime($request['event_id']);
        $multiple_dates = false;
        $current_date = '';
        $dates = '';
        $this->prepareDates($event_dates, $request, $multiple_dates, $current_date, $dates);
        // several dates end

        // $extras = '<audio src="/widget/audio/' . rand(6, 6) . '.mp3" autoplay="true" />';
        // include "superBackground.php";
        // $extras.= $superBackground;
        $marker = $this->getMarker($request['event_id']);
        if ($marker)
            $result['name'] .= ' <div class="specials">
            <span class="specials-marker">' . $marker . '</span></div>';

        $smarty->assign([
            'name' => $result['name'],
            'real_name' => self::getBitrixNameByEventId($request["event_id"]),
            'venue' => $result['venue'],
            'id' => $request['event_id'],
            'date' => $dates, // $result['datestring'], //$this->getTime($request['event_id']),
            'prices' => $prices,
            'image_preview' => $result['image_preview'],
            'image' => $result['image'],
            'widget_url' => self::$widget_url,
            'age' => $result["age"],
            'mode' => ($request["mode"] != "widget") ? 1 : 0,
            'hall' => $hall,
            'version' => $result["version"],
            'current_date' => $current_date,
            'multiple_dates' => $multiple_dates,
            'hallview' => isset($request["zone_id"]) ? 1 : 0,
            'sector_name' => isset($result["sector_name"]) ? $result["sector_name"] : '',
            'insta' => $request['insta'] ? 1 : 0,
            'extras' => $extras,
            'fanticket' => self::isFanticket($request['event_id'])
        ]);
        if ($result["version"] === 0) {
            $smarty->assign("contacts", $result['data']);
        } else {
            if (isset($request["zone_id"]) || $result["version"] == 2)
                $smarty->assign("contacts", $result['prices']);
        }
        $smarty->display(__DIR__ . '/../tpl/' . $this->view . '/index.html');
    }
}